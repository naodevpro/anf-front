import React from "react";

import "./_ourPriorities.scss";
import EuroIcon from "../../../assets/icons/priorities/Euro_icone.svg";
import SanteIcone from "../../../assets/icons/priorities/Sante_Icone.svg";
import SocialIcone from "../../../assets/icons/priorities/Social_Icone.svg";
import PolitiqueEtrangere from "../../../assets/icons/priorities/Politique_etrangere.svg";
import EducationIcon from "../../../assets/icons/priorities/Education_icone.svg";
import FamilleIcon from "../../../assets/icons/priorities/Famille_Icone.svg";

const OurPriorities = () => {
  return (
    <div className="container_global_responsive">
      <div className="container_priorities" id="our-priorities">
        <h2 className="title-2 primary-color mt-3">
          LES PRIORITÉS DU PARTI
        </h2>
        <div className="box_priorities">
          <div className="box_side_left">
            <div className="box_priority">
              <img src={EuroIcon} alt="Europe" />
              <div className="box_priority_description">
                <h3 className="subtitle_priority">Économie</h3>
                <p className="paragraph_priority">
                  Qu'est-ce qui fait l'économie d'un pays ? Ensemble des
                  activités économiques sur son territoire. La croissance
                  économique dépend de l'utilisation des facteurs de production.
                  Ces facteurs de production sont le capital, le travail et la
                  productivité globale des facteurs.
                </p>
              </div>
            </div>
            <div className="box_priority">
              <img src={SanteIcone} alt="Santé" />
              <div className="box_priority_description">
                <h3 className="subtitle_priority">Santé</h3>
                <p className="paragraph_priority">
                  Une population en bonne santé est le moteur d’une économie
                  solide une meilleure santé et une espérance de vie plus longue
                  sont autant d'incitations à investir dans l'éducation, dont
                  les rendements sont alors mécaniquement plus élevés.
                </p>
              </div>
            </div>
            <div className="box_priority">
              <img src={SocialIcone} alt="Social" />
              <div className="box_priority_description">
                <h3 className="subtitle_priority">Social</h3>
                <p className="paragraph_priority">
                  La vie en société permet de se réaliser au contact des autres
                  et de donner un sens à sa vie. C’est d’autant plus important
                  au moment de la retraite. Tous ces liens que nous allons
                  tisser, ces activités dans lesquelles nous allons nous
                  investir, seront la clé de voûte de notre identité sociale.
                </p>
              </div>
            </div>
          </div>
          <div className="box_side_right">
            <div className="box_priority">
              <img src={PolitiqueEtrangere} alt="Politique étrangère" />
              <div className="box_priority_description">
                <h3 className="subtitle_priority">Politique étrangère</h3>
                <p className="paragraph_priority">
                  La politique étrangère de la France, ou diplomatie française,
                  est la politique menée par la France vis-à-vis des autres pays
                  en vue de favoriser ses intérêts géostratégiques, politiques
                  et économiques.
                </p>
              </div>
            </div>
            <div className="box_priority">
              <img src={EducationIcon} alt="Education" />
              <div className="box_priority_description">
                <h3 className="subtitle_priority">Éducation</h3>
                <p className="paragraph_priority">
                  L'éducation contribue au développement des compétences
                  interpersonnelles. L'école est la première occasion structurée
                  pour les enfants de créer des liens avec d'autres enfants de
                  leur âge, avec des règles et des directives pour agir
                  correctement et se traiter les uns les autres.
                </p>
              </div>
            </div>
            <div className="box_priority">
              <img src={FamilleIcon} alt="Famille" />
              <div className="box_priority_description">
                <h3 className="subtitle_priority">Famille</h3>
                <p className="paragraph_priority">
                  Depuis les années 1970, la Cour européenne des droits de
                  l’homme s’est attachée à définir le contenu de la notion de «
                  vie familiale », en élargissant constamment et en modernisant
                  la protection conférée par l’article 8 de la Convention
                  européenne des droits de l’homme.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurPriorities;

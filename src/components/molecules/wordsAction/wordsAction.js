import React from "react";

import "./_wordsAction.scss";

const WordsAction = () => {
  return (
    <div className="container_global_responsive">
      <div className="container_words_action">
        <p>RASSEMBLER</p>
        <p>SOCIALISER</p>
        <p>ÉCOUTER</p>
      </div>
    </div>
  );
};

export default WordsAction;

import React from "react";
import { Link } from "react-router-dom";
import Carousel from "../../atoms/carousel/carousel";
import icon from "../../../assets/icons/arrow_download.svg"

import "./_ourProgram.scss";

const OurProgram = () => {
  return (
    <>
      <div className="container_global_responsive">
        <div className="content">
          <div className="title">
            <h2>
              NOTRE PROGRAMME POUR LA <span>FRANCE</span>
            </h2>
            <p>
              Lorem ipsum, dolor sit amet consectetur adipisicing elit.
              Quibusdam recusandae illum, dolorum eos expedita quisquam earum
              eaque natus nesciunt maxime, vero soluta exercitationem
              consectetur ipsa aperiam ad suscipit impedit optio?
            </p>
          </div>
          <div className="box_link_redirectTo">
            <Link to="/items">
              <h3 className="link_program">>> Découvrir notre programme</h3>
            </Link>

            <p className="download_program">
              <img src={icon} alt="icon" />
              Télécharger notre programme pour la France
            </p>
          </div>
        </div>
      </div>
      <Carousel />
    </>
  );
};

export default OurProgram;

import React, { useState, useRef } from "react";

import Input from "../../atoms/Input/input";
import Button from "../../atoms/Button/button";

import { handleFormError } from "../../../utils/handleFormError";

import "./_formInformation.scss";
import CrossIcon from "../../../assets/icons/cross_icon.png";

const FormInformation = ({ stepFocused, nextStep }) => {
  const [fields, setFields] = useState([
    {
      id: 0,
      type: "text",
      name: "lastname",
      reference: useRef(),
      label: "Nom",
      placeholder: "Ex: Dupont",
      regexp: `^.{1,}$`,
      errorDisplayed: "Veuillez entrer un nom correct",
      error: false,
    },
    {
      id: 1,
      type: "text",
      name: "firstname",
      reference: useRef(),
      label: "Prénom",
      placeholder: "Ex: Jean",
      regexp: `^.{1,}$`,
      errorDisplayed: "Veuillez entrer un prénom correct",
      error: false,
    },
    {
      id: 2,
      type: "email",
      name: "e-mail",
      reference: useRef(),
      label: "E-mail",
      placeholder: "Ex: jean.dupont@monemail.com",
      regexp: /^[a-zA-Z0-9-.]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/,
      errorDisplayed: "Veuillez entrer une adresse e-mail correcte",
      error: false,
    },
    {
      id: 3,
      type: "text",
      name: "nationality",
      reference: useRef(),
      label: "Nationalité",
      placeholder: "Ex: Française",
      regexp: `^.{1,}$`,
      errorDisplayed: "Veuillez entrer nationalité existante",
      error: false,
    },
    {
      id: 4,
      type: "text",
      name: "postalCode",
      reference: useRef(),
      label: "Code postal",
      placeholder: "Ex: 75000",
      regexp: `^.{1,5}$`,
      errorDisplayed: "Veuillez entrer un code postal correct",
      error: false,
    },
    {
      id: 5,
      type: "tel",
      name: "phoneNumber",
      reference: useRef(),
      label: "Numéro de téléphone",
      placeholder: "Ex: 06XXXXXXXX",
      regexp: `(0|\\+33|0033)[1-9][0-9]{8}`,
      error: false,
      errorDisplayed:
        "Veuillez entrer un numéro correct commencant par 06 ou +33",
    },
    {
      id: 6,
      type: "date",
      name: "birthdate",
      reference: useRef(),
      label: "Date de naissance",
      placeholder: "Ex: JJ/MM/AAAA",
      errorDisplayed: "Veuillez entrer une date de naissance correcte",
      error: false,
    },
  ]);

  return (
    <div className="box_form_informations p-1">
      <form className="basic-light-background p-1 mb-3" autoComplete="off">
        <div className="box_form_gender d-flex flex-column align-items-start">
          <label htmlFor="gender" className="label ">
            Genre
          </label>
          <div className="box_radios_gender d-flex m-tb-1">
            <Input
              customClass={"input mb-1"}
              label={"Homme"}
              id={"man"}
              type={"radio"}
              value={"man"}
              name={"gender"}
            />
            <Input
              customClass={"input mb-1"}
              id={"woman"}
              type={"radio"}
              label={"Femme"}
              value={"woman"}
              name={"gender"}
            />
            <Input
              customClass={"input mb-1"}
              id={"woman"}
              type={"radio"}
              label={"Autre"}
              value={"other"}
              name={"gender"}
            />
          </div>
        </div>

        <div className="box_inputs">
          {fields
            ? fields.map((field, index) => {
                return (
                  <div className="d-flex flex-column" key={index}>
                    <Input
                      reference={field.reference}
                      customClass={"input mb-1 input--icon-right"}
                      height={"45px"}
                      width={"100%"}
                      type={field.type}
                      label={field.label}
                      placeholder={field.placeholder}
                      onChangeMethod={handleFormError}
                      onChangeParams={[fields, field, setFields, index]}
                      icon={field.error ? CrossIcon : null}
                    />
                    {field.error ? (
                      <div className="box box-error">
                        <div class="up-triangle up-triangle-error"></div>
                        <span>{field.errorDisplayed}</span>
                      </div>
                    ) : null}
                  </div>
                );
              })
            : null}
        </div>
        <div className="box_general_conditions d-flex align-items-center">
          <div className="box_checkbox">
            <Input
              customClass={"input mb-1"}
              height={"20px"}
              width={"20px"}
              type={"checkbox"}
            />
          </div>
          <span className={"ml-1"}>
            Oui, j'adhère aux conditions générales d'utilisation du site et aux
            règles de fonctionnement de l'ANF
          </span>
        </div>
        <Button
          customClass={"btn btn--squared primary-background basic-light-color"}
          label={"suivant"}
          height={"50px"}
          width={"100%"}
          onClickMethod={nextStep}
          onClickParams={[stepFocused]}
        />
      </form>
    </div>
  );
};

export default FormInformation;

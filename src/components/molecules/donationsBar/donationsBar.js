import React, { useState, useRef } from "react";
import { useHistory } from "react-router-dom";
import Button from "../../atoms/Button/button";
import Input from "../../atoms/Input/input";
import Icon from "../../../assets/icons/arrow.svg";
import IconEuro from "../../../assets/icons/euro_icon.svg";

import SocialNetworksBar from "../../atoms/socialNetworksBar/socialNetworksBar";

import "./_donationsBar.scss";

const DonationsBar = () => {
  let history = useHistory();
  const inputDonnation = useRef({ min: null, max: null, value: null });
  let inputDonnationMin = (inputDonnation.current.min = 10);
  let inputDonnationMax = (inputDonnation.current.max = 7500);
  const [amount, setAmount] = useState("");
  const [inputsStatus, setInputsStatus] = useState([
    {
      inputName: "input-a",
      amount: "20 €",
      realAmount: 20,
      active: false,
    },
    {
      inputName: "input-b",
      amount: "30 €",
      realAmount: 30,
      active: false,
    },
    {
      inputName: "input-c",
      amount: "130 €",
      realAmount: 130,
      active: false,
    },
    {
      inputName: "input-d",
      amount: "300 €",
      realAmount: 300,
      active: false,
    },
  ]);

  const pushAmount = () => {
    history.push(`/donation/${amount}`);
  };

  const handleToggle = (currentInputName) => {
    const updatedInputStatus = inputsStatus.map((input) => {
      if (input.inputName === currentInputName) {
        input.active = true;
        inputDonnation.current.value = "";
        inputDonnation.current.classList.remove(
          "success-blue-border",
          "error-border"
        );
        setAmount(input.realAmount);
      } else {
        input.active = false;
      }
      return input;
    });
    setInputsStatus(updatedInputStatus);
  };

  // change with utils changeClass
  const handleSetAmount = () => {
    if (
      inputDonnation.current.value < inputDonnationMin ||
      inputDonnation.current.value > inputDonnationMax
    ) {
      inputDonnation.current.classList.remove("success-blue-border");
      inputDonnation.current.classList.add("error-border");
    } else {
      inputDonnation.current.classList.remove("error-border");
      inputDonnation.current.classList.add("success-blue-border");
      setAmount(inputDonnation.current.value);
    }
  };

  return (
    <>
      <div className="container_global" id="donations-bar">
        <div className="container_blue"></div>
        <div className="container_form">
          <div className="title">
            <h3>
              <span className="primary-dark-background">
                UN DON POUR TRANSFORMER LA FRANCE
              </span>
            </h3>
          </div>
          <div className="box_dons">
            <div className="all_inputs">
              {inputsStatus.map((label, index) => {
                return (
                  <label
                    key={index}
                    htmlFor={label.inputName}
                    className={
                      label.active ? "donation_label lighted" : "donation_label"
                    }
                    onClick={() => handleToggle(label.inputName)}
                  >
                    {label.amount}
                  </label>
                );
              })}
            </div>
            <div className="box_input_other_amount m-1">
              <Input
                reference={inputDonnation}
                width={"100%"}
                height={"50px"}
                customClass={"input input--icon-right"}
                icon={IconEuro}
                type={"number"}
                placeholder={"Autre montant"}
                onClickMethod={handleToggle}
                onClickParams={["otherInput"]}
                onChangeMethod={handleSetAmount}
                id={"input-other"}
                name={"input-other"}
              />
            </div>
            <Button
              label={"JE DONNE MAINTENANT"}
              height={"55px"}
              width={"100%"}
              icon={Icon}
              customClass={"btn basic-light-color primary-background"}
              onClickMethod={pushAmount}
            />
          </div>
        </div>
        <SocialNetworksBar />
      </div>
    </>
  );
};

export default DonationsBar;

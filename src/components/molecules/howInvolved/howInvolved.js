import React from "react";
import { Link } from "react-router-dom";
import { HashLink } from "react-router-hash-link";

import "./_howInvolved.scss";

import Donation from "../../../assets/illustrations/involved/donation.png";
import Benevole from "../../../assets/illustrations/involved/benevole.png";
import Content from "../../../assets/illustrations/involved/content.png";
import Talk from "../../../assets/illustrations/involved/talk.png";

const HowInvoled = () => {
  var allCases = [
    {
      title: "FAIRE UN DON",
      background: Donation,
      id: "#donations-bar",
    },
    {
      title: "DEVENIR BÉNÉVOLE",
      background: Benevole,
      id: null,
    },
    {
      title: "PARTAGER DU CONTENU",
      background: Content,
      id: null,
    },
    {
      title: "EN PARLER À UNE CONNAISSANCE",
      background: Talk,
      id: null,
    },
  ];

  return (
    <div className="container_global_responsive">
      <div className="container_involved">
        <h2 className="title_involved">
          COMMENT <span>S'ENGAGER</span>
        </h2>
        <p className="description_involved">
          Qu'est ce qu'un engagement politique ? Engagement politique signifie
          "engagement du citoyen au cœur de la cité , par le citoyen". Le
          citoyen n'est pas un sujet qui subit les décisions gouvernementales,
          ils participent à la décision politique. L'engagement politique c'est
          une action individuelle ou collective pour faire évoluer la vie de la
          société. La participation électorale se traduit par le vote lors d'une
          élection.
        </p>
        <div className="box_involved">
          <div className="box_link_redirectTo">
            <Link to="/items">
              <h3 className="link_program">&#187; Découvrir notre programme</h3>
            </Link>
          </div>
          <div className="box_side_left">
            {allCases
              ? allCases.map((caseItem, index) => {
                  if (caseItem.id) {
                    return (
                      <div
                        className="box_involve"
                        id={caseItem.id}
                        style={{
                          backgroundImage: "url(" + caseItem.background + ")",
                        }}
                        key={index}
                      >
                        <HashLink to={caseItem.id} smooth={true} key={index}>
                          <h3 className="involve_text">{caseItem.title}</h3>
                        </HashLink>
                      </div>
                    );
                  } else {
                    return (
                      <div
                        className="box_involve"
                        id={caseItem.id}
                        style={{
                          backgroundImage: "url(" + caseItem.background + ")",
                        }}
                        key={index}
                      >
                        <h3 className="involve_text">{caseItem.title}</h3>
                      </div>
                    );
                  }
                })
              : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default HowInvoled;

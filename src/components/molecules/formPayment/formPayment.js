import React, { useState, useRef } from "react";

import Input from "../../atoms/Input/input";
import Button from "../../atoms/Button/button";

import { handleFormError } from "../../../utils/handleFormError";

import "./_formPayment.scss";
import rectoCard from "../../../assets/illustrations/payment/recto_card.png";
import versoCard from "../../../assets/illustrations/payment/verso_card.png";

import CrossIcon from "../../../assets/icons/cross_icon.png";

// ^(|[1-9][0-9][0-9]?)$
const FormPayment = ({ stepFocused, nextStep }) => {
  const [fields, setFields] = useState([
    {
      id: 0,
      type: "text",
      name: "amount",
      reference: useRef(),
      label: "Montant du don",
      placeholder: "Ex: 20,00€",
      regexp: `^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$`,
      errorDisplayed: "Veuillez entrer un montant entre 1€ et 999€",
      error: false,
      maxLength: 3,
      value: "",
    },
    {
      id: 1,
      type: "text",
      name: "cardNumber",
      reference: useRef(),
      label: "Numéro de carte",
      placeholder: "Ex: 4978 XXXX XXXX XXXX",
      regexp: `^.{1,16}$`,
      errorDisplayed: "Veuillez entrer un numéro de carte valide",
      error: false,
      maxLength: 16,
      value: "",
    },
    {
      id: 2,
      type: "text",
      name: "cardHolder",
      reference: useRef(),
      label: "Titulaire de la carte",
      placeholder: "Ex: DUPONT JEAN",
      regexp: `^.{1,}$`,
      errorDisplayed: "Veuillez entrer un nom dans le champs",
      error: false,
      maxLength: 30,
      value: "",
    },
    {
      id: 3,
      type: "text",
      name: "cardExpiration",
      reference: useRef(),
      label: "Date d'expiration",
      placeholder: "Ex: MM/AA",
      regexp: /\b(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})\b/,
      errorDisplayed:
        "Veuillez entrer une date d'expiration avec un format valide. Ex : MM/AA",
      error: false,
      value: "",
      maxLength: 4,
    },
    {
      id: 4,
      type: "text",
      name: "cardValidationCode",
      reference: useRef(),
      label: "CVV",
      placeholder: "Ex: 089",
      regexp: `^.{1,4}$`,
      errorDisplayed: "Veuillez entrer un code de validation de carte valide ",
      error: false,
      maxLength: 3,
      value: "",
    },
  ]);
  const card = useRef();
  const [isRecto, setIsRecto] = useState(true);
  const [isVerso, setIsVerso] = useState(false);
  const [cardField, setCardField] = useState({
    cardNumber: "",
    cardHolder: "",
    cardExpiration: "",
    cardValidationCode: "",
  });

  const handleChange = (e, field, index) => {
    handleFormError(e, fields, field, setFields, index);
    if (field.name === "cardNumber") {
      return getCardNumber(field, index);
    }
    if (field.name === "cardHolder") {
      return getCardHolder(field, index);
    }
    if (field.name === "cardExpiration") {
      return getCardExpiration(field, index);
    }
    if (field.name === "cardValidationCode") {
      return getCardValidationCode(field, index);
    } else {
      setCardField({
        ...cardField,
        [field.name]: field.reference.current.value,
      });
    }
  };

  const getCardHolder = (field) => {
    setIsRecto(true);
    setIsVerso(false);
    setCardField({
      ...cardField,
      [field.name]: field.reference.current.value,
    });
  };

  const getCardNumber = (field) => {
    setIsRecto(true);
    setIsVerso(false);
    setCardField({
      ...cardField,
      [field.name]: field.reference.current.value
        .replace(/[^\0-9]/g, "")
        .replace(/(.{4})/g, "$1 ")
        .trim(),
    });
  };

  const getCardExpiration = (field) => {
    setIsRecto(true);
    setIsVerso(false);
    setCardField({
      ...cardField,
      [field.name]:
        field.reference.current.value.slice(0, 2) +
        " / " +
        field.reference.current.value.slice(2),
    });
  };

  const getCardValidationCode = (field) => {
    setIsVerso(true);
    setIsRecto(false);
    setCardField({
      ...cardField,
      [field.name]: field.reference.current.value,
    });
  };

  return (
    <div className="box_form_payment p-1">
      <form className="basic-light-background p-1 mb-3 mt-5" autoComplete="off">
        <div className="box_card">
          <div className="card d-flex justify-content-center">
            <img
              ref={card}
              className="card"
              src={isRecto ? rectoCard : isVerso ? versoCard : null}
              alt="card"
            />
            <div className="box_card_infos_recto">
              <div className="box_card_number">
                <h1 className="card_number basic-light-color">
                  {cardField.cardNumber.length < 1 && isRecto
                    ? "XXXX XXXX XXXX XXXX"
                    : isRecto
                    ? cardField.cardNumber
                    : null}
                </h1>
              </div>
              <div className="box_card_sub_infos">
                <h1
                  className="card_holder basic-light-color"
                  style={{
                    fontSize:
                      cardField.cardHolder.length <= 20 ? "16px" : "10px",
                  }}
                >
                  {cardField.cardHolder.length < 1 && isRecto
                    ? "TITULAIRE DE LA CARTE"
                    : isRecto
                    ? cardField.cardHolder.toUpperCase()
                    : null}
                </h1>
                <h1 className="card_expiration basic-light-color">
                  {cardField.cardExpiration.length < 1 && isRecto
                    ? "MM/AA"
                    : isRecto
                    ? cardField.cardExpiration
                    : null}
                </h1>
              </div>
            </div>
            <div className="box_card_infos_verso">
              <h1 className="card_validation_code basic-light-color">
                {cardField.cardValidationCode.length < 1 && isVerso
                  ? "CVV"
                  : isVerso
                  ? cardField.cardValidationCode
                  : null}
              </h1>
            </div>
          </div>
        </div>
        <div className="box_inputs">
          {fields
            ? fields.map((field, index) => {
                return (
                  <div className="d-flex flex-column mt-1" key={index}>
                    <Input
                      reference={field.reference}
                      customClass={"input mb-1 input--icon-right"}
                      height={"45px"}
                      width={"100%"}
                      type={field.type}
                      label={field.label}
                      placeholder={field.placeholder}
                      onChangeMethod={handleChange}
                      onChangeParams={[field, index]}
                      maxLength={field.maxLength ? field.maxLength : null}
                      icon={field.error ? CrossIcon : null}
                    />
                    {field.error ? (
                      <div className="box box-error">
                        <div className="up-triangle up-triangle-error"></div>
                        <span>{field.errorDisplayed}</span>
                      </div>
                    ) : null}
                  </div>
                );
              })
            : null}
        </div>
        <Button
          customClass={"btn btn--squared primary-background basic-light-color"}
          label={"suivant"}
          height={"50px"}
          width={"100%"}
          onClickMethod={nextStep}
          onClickParams={[stepFocused]}
        />
      </form>
    </div>
  );
};

export default FormPayment;

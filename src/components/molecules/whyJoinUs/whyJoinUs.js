import React from "react";

import "./_whyJoinUs.scss";

const WhyJoinUs = () => {
  return (
    <div className="container_global_responsive">
      <div className="container-global">
        <div className="container-img">
          {/* <img src={People} alt="people-img" /> */}
        </div>
        <div className="explanation">
          <h3>Pourquoi nous rejoindre ?</h3>
          <p>
            Des adhérents plus altruistes et idéalistes, des sympathisants plus
            pragmatiques via l’apprentissage politique, se faire des nouvelles
            connaissances et rencontrer des gens avec les mêmes idéaux, En
            effet, être militant c'est consacrer son temps, son énergie, son
            savoir, son expérience, ses compétences, à une organisation sociale.
            De participer aux réunions du parti, de manière à débattre de ses
            orientations, de la stratégie électorale à adopter lors d’une
            élection locale, ou des grands débats nationaux du moment ; faire
            connaître les positions du parti par la distribution de tracts dans
            la rue, sur les marchés ou par la vente d’un journal . de participer
            aux activités lors des campagnes électorales (collage d’affiches,
            organisation de meetings...).
          </p>
        </div>
      </div>
    </div>
  );
};

export default WhyJoinUs;

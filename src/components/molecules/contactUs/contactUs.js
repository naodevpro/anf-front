import React from "react";

import "./_contactUs.scss";

const ContactUs = () => {
  return (
    <>
      <div className="container_global_responsive">
        <div className="container_global_contact" id="contact-us">
          <div className="box_contact">
            <div className="container_map">
              <iframe
                className="map"
                title="adresse-anf"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2623.894683356397!2d2.297135015317538!3d48.87928420718992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66f9510140dbd%3A0x18e5ac164a037b7b!2s58%20Avenue%20de%20Wagram%2C%2075017%20Paris!5e0!3m2!1sfr!2sfr!4v1624921236802!5m2!1sfr!2sfr"
              ></iframe>
              <div className="content">
                <h4 className="title_contact primary-dark-background ">
                  CONTACTEZ-NOUS
                </h4>
                <div className="red_line basic-light-background"></div>
                <div className="adress">
                  <p>L'ALLIANCE POUR UNE NOUVELLE FRANCE</p>
                  <p>58 Avenue de Wagram</p>
                  <p>75017 PARIS</p>
                  <p>08 216 101 55</p>
                </div>
                <div className="red_line basic-light-background"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContactUs;

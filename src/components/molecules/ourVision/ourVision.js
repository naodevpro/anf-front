import React from "react";

import "./_ourVision.scss";

import Presidential from "../../../assets/illustrations/mission/presidential.png";
import European from "../../../assets/illustrations/mission/european.png";
import commitment from "../../../assets/illustrations/mission/engagement.png";
import Step from "../../../assets/illustrations/mission/demarche.png";

const howInvoled = () => {
  var allCases = [
    {
      title: "PROGRAMME PRÉSIDENTIEL",
      background: Presidential,
    },
    {
      title: "PROGRAMME EUROPÉEN",
      background: European,
    },
    {
      title: "NOS ENGAGEMENTS",
      background: commitment,
    },
    {
      title: "NOTRE DÉMARCHE",
      background: Step,
    },
  ];

  return (
    <div className="container_global_responsive">
      <div className="container_mission" id="our-vision">
        <h2 className="title_mission">
          NOTRE VISION POUR LA FRANCE
        </h2>
        <p className="description_mission">
          Une société décloisonnée et ouverte sur le monde, la prospérité de la
          France passe par un décloisonnement de la société. De nouvelles
          mobilités sont à construire pour les jeunes, les seniors et les
          entreprises. Un modèle social qui préconise de reconstruire le plein
          emploi et d'améliorer la qualité du travail. Les politiques sociales
          doivent être simplifiées et personnalisées.
        </p>
        <div className="box_mission">
          <div className="box_side_left">
            {allCases
              ? allCases.map((caseItem, index) => {
                  return (
                    <div
                      className="box_involve"
                      style={{
                        backgroundImage: "url(" + caseItem.background + ")",
                      }}
                      key={index}
                    >
                      <h3 className="involve_text">{caseItem.title}</h3>
                    </div>
                  );
                })
              : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default howInvoled;

import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

import api from "../../../utils/api";
import renderNotification from "../../../utils/notification";

import "./_formWizard.scss";

import FormItem from "../../atoms/formItem/formItem";
import FormConstat from "../../atoms/formConstat/formConstat";
import FormObjectif from "../../atoms/formObjectif/formObjectif";
import FormArgument from "../../atoms/formArgument/formArgument";

const FormWizard = ({ stepFocused, setStepFocused }) => {
  const dispatch = useDispatch();
  const form = useRef(null);

  const itemState = useSelector((state) => state.item.item);
  const constatState = useSelector((state) => state.constat.constat);
  const objectifState = useSelector((state) => state.objectif.objectif);
  const argumentState = useSelector((state) => state.argument.argument);

  const [isItemform, setIsItemForm] = useState(true);
  const [isConstatform, setIsConstatForm] = useState(false);
  const [isObjectifForm, setIsObjectifForm] = useState(false);
  const [isArgumentForm, setIsArgumentForm] = useState(false);

  const [itemSuccess, setItemSuccess] = useState(null);
  const [constatSuccess, setConstatSuccess] = useState(null);
  const [objectifSuccess, setObjectifSuccess] = useState(null);
  const [argumentSuccess, setArgumentSuccess] = useState(null);

  const [open, setOpen] = useState(false);

  const [item, setItem] = useState({
    title: "",
    shortDescription: "",
    headerAsset: "",
    titleLongDescription: "",
    longDescription: "",
  });

  const [constat, setConstat] = useState({
    titleConstat: "",
    contentConstat: "",
    assetConstat: "",
    itemId: "",
  });

  const [objectif, setObjectif] = useState({
    titleObjectif: "",
    descriptionObjectif: "",
    itemId: "",
  });

  const [argument, setArgument] = useState({
    titleArgument: "",
    descriptionArgument: "",
    objectifId: "",
  });

  const handleToConstats = async (e) => {
    setIsItemForm(false);
    setIsConstatForm(true);
  };

  const handleToObjectifs = (e) => {
    setIsItemForm(false);
    setIsConstatForm(false);
    setIsObjectifForm(true);
  };

  const handleToArguments = (e) => {
    setIsItemForm(false);
    setIsConstatForm(false);
    setIsObjectifForm(false);
    setIsArgumentForm(true);
  };

  const handleToFinish = (e) => {
    setIsItemForm(false);
    setIsConstatForm(false);
    setIsObjectifForm(false);
    setIsArgumentForm(true);
  };

  const submitFormData = async (e, path) => {
    e.preventDefault();
    try {
      const data = new FormData(form.current);
      const result = await api.post(path, data);
      setOpen(true);
      switch (path) {
        case "/items":
          dispatch({
            type: "ITEM_SET_ADMIN",
            payload: result.data.data,
          });
          switchToForm(path);
          break;
        case "/constats":
          dispatch({
            type: "CONSTAT_SET",
            payload: result.data.data,
          });
          switchToForm(path);
          dispatch({ type: "ITEM_RESET" });
          break;
        default:
          break;
      }
    } catch (err) {
      switch (path) {
        case "/items":
          dispatch({
            type: "ITEM_ERROR",
            payload: err,
          });
          setOpen(true);
          return setItemSuccess(false);
        case "/constats":
          dispatch({
            type: "CONSTAT_ERROR",
            payload: err,
          });
          setOpen(true);
          return setConstatSuccess(false);
        default:
          break;
      }
    }
  };

  const submitForm = async (e, path, data, id) => {
    e.preventDefault();
    try {
      switch (path) {
        case "/objectifs":
          const objectif = await api.post(path, {
            ...data,
            itemId: id,
          });
          setOpen(true);
          dispatch({
            type: "OBJECTIF_SET",
            payload: objectif.data.data,
          });
          switchToForm(path);
          break;
        case "/arguments":
          const argument = await api.post(path, {
            ...data,
            objectifId: id,
          });
          setOpen(true);
          dispatch({
            type: "ARGUMENT_SET",
            payload: argument.data.data,
          });
          switchToForm(path);
          break;
        default:
          break;
      }
    } catch (err) {
      switch (path) {
        case "/objectifs":
          dispatch({
            type: "OBJECTIF_ERROR",
            payload: err,
          });
          setOpen(true);
          return setObjectifSuccess(false);
        case "/arguments":
          dispatch({
            type: "ARGUMENT_ERROR",
            payload: err,
          });
          setOpen(true);
          return setArgumentSuccess(false);
        default:
          break;
      }
    }
  };

  const switchToForm = (path) => {
    switch (path) {
      case "/items":
        setItemSuccess(true);
        handleToConstats();
        setStepFocused(1);

        break;
      case "/constats":
        setConstatSuccess(true);
        handleToObjectifs();
        setStepFocused(2);

        break;
      case "/objectifs":
        setObjectifSuccess(true);
        handleToArguments();
        setStepFocused(3);
        break;
      case "/arguments":
        setArgumentSuccess(true);
        handleToFinish();
        break;
      default:
        break;
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div className="container_form_wizard">
      {open && itemSuccess
        ? renderNotification(
            200,
            "Succès",
            "L'item à bien été enregistré ✅",
            handleClose
          )
        : null}
      {open && constatSuccess
        ? renderNotification(
            200,
            "Succès",
            "Le constat à bien été enregistré ✅",
            handleClose
          )
        : null}
      {open && objectifSuccess
        ? renderNotification(
            200,
            "Succès",
            "L'objectif à bien été enregistré ✅",
            handleClose
          )
        : null}
      {open && argumentSuccess
        ? renderNotification(
            200,
            "Succès",
            "L'argument à bien été enregistré ✅",
            handleClose
          )
        : null}
      {open && itemSuccess === false
        ? renderNotification(
            400,
            "Erreur",
            itemState.values.response
              ? itemState.values.response.data.message.message
              : itemState.values.message,
            handleClose
          )
        : null}
      {open && constatSuccess === false
        ? renderNotification(
            400,
            "Erreur",
            constatState.values.response
              ? constatState.values.response.data.message.message
              : constatState.values.message,
            handleClose
          )
        : null}
      {open && objectifSuccess === false
        ? renderNotification(
            400,
            "Erreur",
            objectifState.values.response
              ? objectifState.values.response.data.message.message
              : objectifState.values.message,
            handleClose
          )
        : null}
      {open && argumentSuccess === false
        ? renderNotification(
            400,
            "Erreur",
            argumentState.values.response
              ? argumentState.values.response.data.message.message
              : argumentState.values.message,
            handleClose
          )
        : null}
      {isItemform && stepFocused === 0 ? (
        <FormItem
          form={form}
          item={item}
          setItem={setItem}
          submit={submitFormData}
          scrollToTop={scrollToTop}
        />
      ) : null}
      {isConstatform && stepFocused === 1 ? (
        <FormConstat
          form={form}
          constat={constat}
          setConstat={setConstat}
          itemId={itemState.values.id}
          submit={submitFormData}
          scrollToTop={scrollToTop}
        />
      ) : null}

      {isObjectifForm && stepFocused === 2 ? (
        <FormObjectif
          objectif={objectif}
          setObjectif={setObjectif}
          itemId={constatState.values.itemId}
          submit={submitForm}
          scrollToTop={scrollToTop}
        />
      ) : null}
      {isArgumentForm && stepFocused === 3 ? (
        <FormArgument
          argument={argument}
          setArgument={setArgument}
          objectifId={objectifState.values.id}
          submit={submitForm}
          scrollToTop={scrollToTop}
        />
      ) : null}
    </div>
  );
};

export default FormWizard;

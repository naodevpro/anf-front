import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import api from "../../../utils/api";
import { HashLink } from "react-router-hash-link";
import Logo from "../../../assets/logos/logo_anf_white.png";
import "./_navigation.scss";

import Button from "../../atoms/Button/button";

const Nav = () => {
  const appState = useSelector((state) => state);
  const items = appState.items.items.values;
  const [open, setOpen] = useState(true);
  const [heightNav, setHeightNav] = useState("0%");

  const toggleNav = () => {
    setOpen(!open);
    if (open === true) {
      setHeightNav("100%");
    } else {
      setHeightNav("0%");
    }
  };

  const setOpenNav = () => {
    setOpen(true);
    toggleNav();
  };

  const handleLogout = async () => {
    await api.post("/logout");
    window.location.reload();
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div className="container_global_responsive_nav">
      <nav>
        {appState ? (
          <>
            <Link to="/" onClick={() => scrollToTop()}>
              <div className="box_logo">
                <img src={Logo} alt="logo" />
              </div>
            </Link>
            <div
              className={!open ? "nav_burger open" : "nav_burger"}
              onClick={() => toggleNav()}
            >
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
            <div id="myNav" className="overlay" style={{ height: heightNav }}>
              <div className="links">
                <HashLink
                  to="/#our-priorities"
                  smooth={true}
                  onClick={() => {
                    setOpenNav();
                  }}
                >
                  NOS PRIORITÉS
                </HashLink>
                <HashLink
                  to="/#our-vision"
                  smooth={true}
                  onClick={() => {
                    setOpenNav();
                  }}
                >
                  NOTRE VISION
                </HashLink>
                <Link
                  to="/items"
                  onClick={() => {
                    setOpenNav();
                  }}
                >
                  LE PROGRAMME
                </Link>
                <HashLink
                  to="/#contact-us"
                  smooth={true}
                  onClick={() => {
                    setOpenNav();
                  }}
                >
                  CONTACTEZ-NOUS
                </HashLink>
                {items
                  ? items.map((item, index) => {
                    if (item.title === "Covid-19") {
                      return (
                        <div key={index}>
                          <Link
                            to={`/item/${item.id}`}
                            className="link_covid"
                            onClick={() => {
                              setOpenNav();
                            }}
                          >
                            {item.title.toUpperCase()}
                          </Link>
                        </div>
                      );
                    }
                    return null;
                  })
                  : null}
                <Link
                  to="/login"
                  onClick={() => {
                    setOpenNav();
                  }}
                >
                  {appState.auth.user.values &&
                    appState.auth.user.values.role === "admin" ? null : (
                    <Button
                      label={"CONNEXION"}
                      height={"40px"}
                      width={"250px"}
                      customClass={
                        "btn btn--rounded basic-light-background primary-dark-color"
                      }
                      onClickMethod={setOpenNav}
                    />
                  )}
                </Link>
                {appState.auth.user.values &&
                  appState.auth.user.values.role === "admin" ? (
                  <Button
                    label={"DECONNEXION"}
                    height={"40px"}
                    width={"250px"}
                    customClass={
                      "btn btn--rounded basic-light-color primary-dark-background"
                    }
                    onClickMethod={handleLogout}
                  />
                ) : (
                  <HashLink
                    to="/#donations-bar"
                    smooth={true}
                    onClick={() => {
                      setOpenNav();
                    }}
                  >
                    <Button
                      label={"JE DONNE MAINTENANT"}
                      height={"40px"}
                      width={"250px"}
                      customClass={
                        "btn btn--rounded basic-light-color primary-dark-background"
                      }
                    />
                  </HashLink>
                )}
              </div>
            </div>
            <div className="nav_links_desktop">
              <HashLink to="/#our-priorities" smooth={true}>
                <h2>NOS PRIORITÉS</h2>
              </HashLink>
              <HashLink to="/#our-vision" smooth={true}>
                <h2>NOTRE VISION</h2>
              </HashLink>
              <Link to="/items">
                <h2>LE PROGRAMME</h2>
              </Link>
              <HashLink to="/#contact-us" smooth={true}>
                <h2>CONTACTEZ-NOUS</h2>
              </HashLink>
              {items
                ? items.map((item, index) => {
                  if (item.title === "Covid-19") {
                    return (
                      <div key={index}>
                        <Link to={`/item/${item.id}`}>
                          <h2 className="link_covid">
                            {item.title.toUpperCase()}
                          </h2>
                        </Link>
                      </div>
                    );
                  }
                  return null;
                })
                : null}
            </div>
            <div className="box_buttons">
              <Link to="/login">
                {appState.auth.user.values &&
                  appState.auth.user.values.role === "admin" ? null : (
                  <Button
                    label={"CONNEXION"}
                    height={"30px"}
                    width={"170px"}
                    customClass={
                      "btn btn--rounded basic-light-background primary-dark-color"
                    }
                  />
                )}
              </Link>
              {appState.auth.user.values &&
                appState.auth.user.values.role === "admin" ? (
                <Button
                  label={"DECONNEXION"}
                  height={"30px"}
                  width={"150px"}
                  customClass={
                    "btn btn--rounded basic-light-color primary-dark-background"
                  }
                  onClickMethod={handleLogout}
                />
              ) : (
                <HashLink to="/#donations-bar" smooth={true}>
                  <Button
                    label={"FAIRE UN DON"}
                    height={"30px"}
                    width={"170px"}
                    customClass={
                      "btn btn--rounded basic-light-color primary-dark-background m-lr-1"
                    }
                  />
                </HashLink>
              )}
            </div>
          </>
        ) : null}
      </nav>
    </div>
  );
};

export default Nav;

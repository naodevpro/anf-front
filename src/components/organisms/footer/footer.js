import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { HashLink } from "react-router-hash-link";

import SocialNetworksBar from "../../atoms/socialNetworksBar/socialNetworksBar";
import Logo from "../../../assets/logos/logo_anf_white.png";
import bulletinAdhesion from "../../../assets/pdfs/adhesion.pdf";

import "./_footer.scss";

const Footer = () => {
  const appState = useSelector((state) => state.items.items.values);

  return (
    <>
      <div className="container_global_responsive">
        <SocialNetworksBar />
        <footer>
          <img src={Logo} alt="logo_anf_white" className="logo_anf" />
          <div className="all_links">
            <div>
              <h5>Notre vision</h5>
              <p>Programme présidentiel</p>
              <p>Programme Européen</p>
              <p>Nos engagements</p>
              <p>Nos démarches</p>
            </div>
            <div>
              <h5>Le programme</h5>
              {appState
                ? appState.map((item, index) => {
                    return (
                      <div key={index}>
                        <Link to={`/item/${item.id}`}>
                          <p>
                            {item.title.charAt(0).toUpperCase() +
                              item.title.slice(1)}
                          </p>
                        </Link>
                      </div>
                    );
                  })
                : null}
            </div>
            <div>
              <h5>Comment s'engager ?</h5>
              <p>Faire un don</p>
              <p>Devenir bénévole</p>
              <p>Partager nos contenus</p>
              <p>Inviter une connaissance</p>
            </div>
          </div>
          <div className="box_dons">
            <p>
              Vous pouvez
              <HashLink
                to="/#donations-bar"
                smooth={true}
                className="link-donations-bar"
              >
                <span>donner en ligne</span>
              </HashLink>
              ou en
              <a href={bulletinAdhesion} download="adhesion.pdf">
                <span>téléchargeant ce bulletin</span>
              </a>
              .
            </p>
          </div>
          <div className="box_contact">
            <HashLink to="/#contact-us" smooth={true}>
              <p>Contactez-nous</p>
            </HashLink>
            <p>Mes données personnelles</p>
            <Link to="/legalNotices">
              <p>Mentions légales</p>
            </Link>
          </div>
          <div className="box_protection">
            <p>Politique des cookies</p>
            <p>Politique des protections de données</p>
          </div>
        </footer>
      </div>
    </>
  );
};

export default Footer;

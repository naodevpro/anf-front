import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import { Link } from "react-router-dom";
import { HashLink } from "react-router-hash-link";
import BoxNotification from "../../atoms/boxNotification/boxNotification";
import Button from "../../atoms/Button/button"
import Map from "../../../assets/illustrations/map.svg";

import "./_header.scss";

const Header = () => {
  const appState = useSelector((state) => state);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    appState.auth.user.isLogged ? setOpen(true) : setOpen(false);
  }, [appState]);

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div className="container_global_responsive">
      {open ? (
        <BoxNotification
          code={200}
          titleNotification={"Connexion réussie"}
          descriptionNotification={`Bienvenue à toi ${appState.auth.user.values.firstname} 👋`}
          handleClose={handleClose}
        />
      ) : null}
      <header>
        <div className="heading_left">
          <h4 className="subtitle_heading basic-light-color">
            AVEC ALLIANCE POUR UNE NOUVELLE FRANCE, NOUS GARDERONS
          </h4>
          <div className="red_separation"></div>
          <h1 className="title-heading basic-light-color">
            ENSEMBLE NOUS SOMMES
            <span className="title_heading_bolded"> PLUS FORT</span>
          </h1>
          <div className="red_separation"></div>
          <h4 className="subtitle_heading basic-light-color">
            LE PARTI POLITIQUE À L'ÉCOUTE DES FRANÇAIS
          </h4>
          <p className="subtext_heading basic-light-color">
            L'ANF l'Alliance pour une nouvelle France est un parti politique à
            l'écoute des français. Ce qui préoccupe réellement les Français le
            résulat est sans appel. La plupart des Français souhaite une
            amélioration de leurs services publics et de leur pouvoir d’achat.
          </p>
          <div className="box_call_to_actions">
            <HashLink
              to="#donations-bar"
              smooth={true}
            >
              <Button
                label={"Faire un don"}
                height={"50px"}
                width={"250px"}
                customClass={
                  "btn btn--rounded primary-color basic-light-background"
                }
              />
            </HashLink>
            <Link
              to="/join"
            >
              <Button
                label={"Adhérer"}
                height={"50px"}
                width={"250px"}
                customClass={
                  "btn btn--rounded basic-light-color primary-background"
                }
              />
            </Link>
          </div>
        </div>
        <div className="heading_right">
          <img src={Map} alt="carte de france" className="map" />
        </div>
      </header>
    </div>
  );
};

export default Header;

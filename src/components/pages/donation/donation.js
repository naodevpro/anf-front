import React, { useState } from "react";
import { Link } from "react-router-dom";

import { scrollToTopOnClick } from "../../../utils/scrollToTopOnClick";

import FormInformation from "../../molecules/formInformation/formInformation";
import FormPayment from "../../molecules/formPayment/formPayment";

import BreadCrumb from "../../atoms/Breadcrumb/breadCrumb";

import "./_donation.scss";
import downloadIcon from "./../../../assets/icons/arrow_download_white.svg";

import logoWhite from "../../../assets/logos/logo_anf_white.png";

const Payment = () => {
  const [steps] = useState([
    {
      title: "Informations",
      success: false,
    },
    {
      title: "Paiement",
      success: false,
    },
    {
      title: "Récapitulatif",
      success: false,
    },
  ]);

  const [stepFocused, setStepFocused] = useState(0);

  const nextStep = (e, step) => {
    e.preventDefault();
    console.log(e);
    console.log(step);
    setStepFocused(step + 1);
    scrollToTopOnClick();
  };

  return (
    <div className="container_responsive">
      <div className="container_donation">
        <nav className="p-3">
          <Link to="/">
            <div className="box_logo">
              <img src={logoWhite} alt="logo_white" className="logo_white" />
            </div>
          </Link>
          <div className="box_breadcrumb">
            <BreadCrumb
              steps={steps}
              stepFocused={stepFocused}
              nextStep={nextStep}
            />
          </div>
        </nav>
        <div className="sub_container">
          <div className="box_text_action d-flex flex-column align-items-center justify-content-center mb-2">
            <h1 className="title-1 basic-light-color mt-2">
              Faites un don et mener des actions concrètes pour la France !
            </h1>
            <p className="subtitle basic-light-color mt-1">
              Rendons les français fiers de leur pays. Chaque don compte et sera
              prit en compte, avec la mise en place d'actions concrètes pour la
              France.
            </p>
            <p className="download_join basic-light-color d-flex align-items-center justify-content-center">
              <img src={downloadIcon} alt="icon" />
              Vous pouvez également télécharger le bulletin d'adhésion au format
              PDF.
            </p>
          </div>
          {stepFocused === 0 ? (
            <FormInformation stepFocused={stepFocused} nextStep={nextStep} />
          ) : null}
          {stepFocused === 1 ? (
            <FormPayment stepFocused={stepFocused} nextStep={nextStep} />
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Payment;

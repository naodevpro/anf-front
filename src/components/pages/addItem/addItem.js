import React, { useState } from "react";
import { Link } from "react-router-dom";

import BreadCrumb from "../../atoms/Breadcrumb/breadCrumb";
import FormWizard from "../../molecules/formWizard/formWizard";

import "./_addItem.scss";
import logoWhite from "./../../../assets/logos/logo_anf_white.png";

function AddItem() {
  const [steps] = useState([
    {
      title: "Item",
      success: false,
    },
    {
      title: "Constat",
      success: false,
    },
    {
      title: "Objectif",
      success: false,
    },
    {
      title: "Argument",
      success: false,
    },
  ]);
  const [stepFocused, setStepFocused] = useState(0);

  return (
    <div className="container_global_additem">
      <div className="background_image">
        <nav>
          <Link to="/">
            <figure className="box_logo">
              <img src={logoWhite} alt="logo_white" className="logo_white" />
            </figure>
          </Link>
          <div className="box_breadcrumb">
            <BreadCrumb steps={steps} stepFocused={stepFocused} />
          </div>
        </nav>
        <section className="box_form">
          <article className="box_for_text">
            <h2>
              Parce que chaque thème est important pour une nouvelle France !
            </h2>
            <p>
              Grâce à cette interface, il vous est facile de gérer les items
              présent sur le site-web ! N'hésitez pas à faire remonter à
              l'équipe technique le moindre problème rencontrés afin que ceux-là
              soit mis à jour au plus vite !
            </p>
          </article>
          <article className="box_for_form">
            <FormWizard
              stepFocused={stepFocused}
              setStepFocused={setStepFocused}
            />
          </article>
        </section>
      </div>
    </div>
  );
}

export default AddItem;

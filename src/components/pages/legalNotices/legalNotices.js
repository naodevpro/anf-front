import React from "react";

import "./_legalNotices.scss";

const LegalNotices = () => {
  return (
    <div className="container_legal_notices">
      <h1>Mentions légales</h1>
      <h2>Mentions légales du site</h2>
      <h3>Définitions</h3>
      <p>
        <strong className="strong-right">Le site :</strong>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://alliancenouvellefrance.fr/"
        >
          http://alliancenouvellefrance.fr
        </a>
      </p>
      <p>
        <strong className="strong-right">Éditeur :</strong>
        personne morale ou physique chargée par le propriétaire du site de la
        maintenance éditoriale du site.
      </p>
      <p>
        <strong className="strong-right">Utilisateur :</strong> Internaute se
        connectant, utilisant le site susnommé.
      </p>
      <p>
        <strong className="strong-right">Contenu :</strong>
        Ensemble des éléments constituants l’information présente sur le site,
        notamment textes – images – vidéos.
      </p>
      <p>
        <strong className="strong-right">Prestations et Services :</strong>
        le site met à disposition des Utilisateurs.
      </p>
      <p>
        <strong className="strong-right">Informations utilisateurs :</strong>
        Ci après dénommé &#171; Information (s) &#187; qui correspondent à
        l’ensemble des données personnelles susceptibles d’être détenues par le
        site pour la gestion de votre compte, de la gestion de la relation des
        utilisateurs et à des fin d’analyses et de statistiques.
      </p>
      <p>
        <strong className="strong-right">Informations personnelles :</strong>&#171;
        Les informations qui permettent, sous quelque forme que ce soit,
        directement ou non, l’identification des personnes physiques auxquelles
        elles s’appliquent &#187; (article 4 de la loi n° 78-17 du 6 janvier
        1978).
      </p>
      <p>
        Les termes &#171; données à caractère personnel &#187;, &#171; personne
        concernée &#187;, &#171; sous traitant &#187; et &#171; données
        sensibles &#187; ont le sens défini par le
        <a
          className="a-left"
          target="_blank"
          rel="noopener noreferrer"
          href="https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32016R0679"
        >
          Règlement Général sur la Protection des Données (RGPD : n° 2016-679).
        </a>
      </p>
      <h3>1. Présentation du site internet</h3>
      <p>
        En vertu de l’article 6 de la loi n° 2004-575 du 21 juin 2004 pour la
        confiance dans l’économie numérique, il est précisé aux utilisateurs du
        site internet
        <a
          className="a-left-right"
          target="_blank"
          rel="noopener noreferrer"
          href="https://rassemblementnational.fr/"
        >
          https://www.rassemblementnational.fr
        </a>
        l’identité des différents intervenants dans le cadre de sa réalisation
        et de son suivi:
      </p>
      <p>
        <strong className="strong-right">Propriétaire :</strong> ALLIANCE POUR UNE
        NOUVELLE FRANCE
      </p>
      <p>58 AVENUE DE WAGRAM – 75017 PARIS – France</p>
      <p>
        <strong className="strong-right">Téléphone :</strong>08 21 61 01 55
      </p>
      <p>
        <strong className="strong-right">SIRET :</strong>898 138 607 00012
      </p>
      <p>
        <strong className="strong-right">Responsable publication :</strong> Phillipe
        JOMIE
      </p>
      <p>
        <strong className="strong-right">Co-responsable de publication :</strong>
        Lydie GUARINOS
      </p>
      <p>
        <strong className="strong-right">Hébergeur :</strong>OVH – 2 rue Kellermann,
        59100 Roubaix 1007 – Site internet :
        <a
          className="a-left"
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.ovhcloud.com/fr/"
        >
          ovh.com
        </a>
      </p>
      <p>
        <strong className="strong-right">
          Délégué à la protection des données :
        </strong>
        Lydie GUARINOS
      </p>
      <h3>
        2. Conditions générales d’utilisation du site et des services proposés.
      </h3>
      <p>
        Le Site constitue une œuvre de l’esprit protégée par les dispositions du
        Code de la Propriété Intellectuelle et des Réglementations
        Internationales applicables. L’Utilisateur ne peut en aucune manière
        réutiliser, céder ou exploiter pour son propre compte tout ou partie des
        éléments ou travaux du Site.
      </p>
      <p>
        L’utilisation du site implique l’acceptation pleine et entière des
        conditions générales d’utilisation ci-après décrites. Ces conditions
        d’utilisation sont susceptibles d’être modifiées ou complétées à tout
        moment, les utilisateurs du site sont donc invités à les consulter de
        manière régulière.
      </p>
      <p>
        Ce site internet est normalement accessible à tout moment aux
        utilisateurs. Une interruption pour raison de maintenance technique peut
        être toutefois décidée par le directeur de la publication, qui
        s’efforcera alors de communiquer préalablement aux utilisateurs les
        dates et heures de l’intervention. Le site est mis à jour régulièrement.
        De la même façon, les mentions légales peuvent être modifiées à tout
        moment : elles s’imposent néanmoins à l’utilisateur qui est invité à s’y
        référer le plus souvent possible afin d’en prendre connaissance.
      </p>
      <h3>3. Description des services fournis.</h3>
      <p>
        Le site internet a pour objet de fournir une information concernant
        l’ensemble des activités de l'Alliance pour une Nouvelle France, qui
        s’efforce de fournir sur le site des informations aussi précises que
        possible. Toutefois, il ne pourra être tenu responsable des oublis, des
        inexactitudes et des carences dans la mise à jour, qu’elles soient de
        son fait ou du fait des tiers partenaires qui lui fournissent ces
        informations.
      </p>
      <p>
        Toutes les informations indiquées sur le site sont données à titre
        indicatif, et sont susceptibles d’évoluer. Par ailleurs, les
        renseignements figurant sur le site ne sont pas exhaustifs. Ils sont
        donnés sous réserve de modifications ayant été apportées depuis leur
        mise en ligne.
      </p>
      <h3>4. Limitations contractuelles sur les données techniques.</h3>
      <p>
        Le site utilise la technologie JavaScript. Le site Internet ne pourra
        être tenu responsable de dommages matériels liés à l’utilisation du
        site. Aussi, l’utilisateur du site s’engage à accéder au site en
        utilisant un matériel récent, ne contenant pas de virus et avec un
        navigateur de dernière génération mis-à-jour. Le site est hébergé chez
        un prestataire sur le territoire Français, conformément aux dispositions
        du Règlement Général sur la Protection des Données (RGPD : n° 2016-679).
      </p>
      <p>
        L’objectif est d’apporter une prestation qui assure le meilleur taux
        d’accessibilité. L’hébergeur assure la continuité de son service 24
        Heures sur 24, tous les jours de l’année. Il se réserve néanmoins la
        possibilité d’interrompre le service d’hébergement pour les durées les
        plus courtes possibles notamment à des fins de maintenance,
        d’amélioration de ses infrastructures, de défaillance de ses
        infrastructures ou si les Prestations et Services génèrent un trafic
        réputé anormal.
      </p>
      <p>
        Le site et l’hébergeur ne pourront être tenus responsables en cas de
        dysfonctionnement du réseau Internet, des lignes téléphoniques ou du
        matériel informatique et de téléphonie lié notamment à l’encombrement du
        réseau empêchant l’accès au serveur.
      </p>
      <h3>5. Propriété intellectuelle et contrefaçons.</h3>
      <p>
        L'Alliance pour une Nouvelle France, il est propriétaire des droits de
        propriété intellectuelle et détient les droits d’usage sur tous les
        éléments accessibles sur le site internet, notamment les textes, images,
        graphismes, logos, vidéos, icônes et sons. Toute reproduction,
        représentation, modification, publication, adaptation de tout ou partie
        des éléments du site, quel que soit le moyen ou le procédé utilisé, est
        interdite, sauf autorisation écrite préalable du directeur de la
        publication.
      </p>
      <p>
        Toute exploitation non autorisée du site ou de l’un quelconque des
        éléments qu’il contient sera considérée comme constitutive d’une
        contrefaçon et poursuivie conformément aux dispositions des articles
        L.335-2 et suivants du Code de Propriété Intellectuelle.
      </p>
      <h3>6. Limitations de responsabilité.</h3>
      <p>
        L'Alliance pour une Nouvelle France agit en tant qu’éditeur du site,
        soit directement, soit en confiant l’édition à des partenaires dûment
        mandatés. Il est responsable de la qualité et de la véracité du Contenu
        qu’il publie.
      </p>
      <p>
        Il ne pourra être tenu responsable des dommages directs et indirects
        causés au matériel de l’utilisateur, lors de l’accès au site internet,
        et résultant soit de l’utilisation d’un matériel ne répondant pas aux
        spécifications indiquées au point 4, soit de l’apparition d’un bug ou
        d’une incompatibilité.
      </p>
      <p>
        Il ne pourra également être tenu responsable des dommages indirects
        (tels par exemple qu’une perte de marché ou perte d’une chance)
        consécutifs à l’utilisation du site. Des espaces interactifs
        (possibilité de poser des questions dans l’espace contact) sont à la
        disposition des utilisateurs. L’éditeur se réserve le droit de
        supprimer, sans mise en demeure préalable, tout contenu déposé dans cet
        espace qui contreviendrait à la législation applicable en France, en
        particulier aux dispositions relatives à la protection des données. Le
        cas échéant, le Rassemblement National se réserve également la
        possibilité de mettre en cause la responsabilité civile et/ou pénale de
        l’utilisateur, notamment en cas de message à caractère raciste,
        injurieux, diffamant, ou pornographique, quel que soit le support
        utilisé (texte, photographie …).
      </p>
      <h3>7. Gestion des données personnelles.</h3>
      <p>
        L’Utilisateur est informé des réglementations concernant la
        communication marketing, la loi du 21 Juin 2014 pour la confiance dans
        l’Économie Numérique, la Loi Informatique et Liberté du 06 Août 2004
        ainsi que du Règlement Général sur la Protection des Données (RGPD : n°
        2016-679).
      </p>
      <p>
        Conformément à la loi &ldquo;Informatique et Libertés&rdquo;, le
        traitement des informations nominatives relatives aux internautes a fait
        l’objet d’une déclaration auprès de la Commission Nationale de
        l’Informatique et des Libertés (CNIL) sous le n°2010753v0.
      </p>
      <h4>7.1 Responsables de la collecte des données personnelles</h4>
      <p>
        Pour les Données Personnelles collectées dans le cadre de la création du
        compte personnel de l’Utilisateur et de sa navigation sur le Site, le
        responsable du traitement des Données Personnelles est le Délégué à la
        protection des données mentionné au premier chapitre des présentes.
      </p>
      <p>
        En tant que responsable du traitement des données qu’il collecte, il
        s’engage à respecter le cadre des dispositions légales en vigueur. Il
        lui appartient notamment d’établir les finalités de ses traitements de
        données, de fournir à ses utilisateurs, à partir de la collecte de leurs
        consentements, une information complète sur le traitement de leurs
        données personnelles et de maintenir un registre des traitements
        conforme à la réalité. Chaque fois qu’il traite des Données
        Personnelles, il prend toutes les mesures raisonnables pour s’assurer de
        l’exactitude et de la pertinence des Données Personnelles au regard des
        finalités pour lesquelles il les traite.
      </p>
      <h4>7.2 Finalité des données collectées</h4>
      <p>
        Le responsable du traitement des Données Personnelles est susceptible de
        traiter tout ou partie des données :
      </p>
      <ul>
        <li>
          pour permettre la navigation sur le Site et la gestion et la
          traçabilité des prestations et services fournis à l’utilisateur :
          données de connexion et d’utilisation du Site, signature de pétition,
          requête via le formulaire de contact, etc.
        </li>
        <li>
          pour prévenir et lutter contre la fraude informatique (spamming,
          hacking…) : matériel informatique utilisé pour la navigation,
          l’adresse IP, le mot de passe (hashé)
        </li>
        <li>
          pour améliorer la navigation sur le Site : données de connexion et
          d’utilisation
        </li>
        <li>
          pour mener des enquêtes de satisfaction facultatives sur le site :
          adresse email
        </li>
        <li>
          pour mener des campagnes de communication (sms, mail) : nom, numéro de
          téléphone, adresse email
        </li>
      </ul>
      <p>
        L'Alliance pour une Nouvelle France ne commercialise pas vos données
        personnelles qui sont donc uniquement utilisées par nécessité ou à des
        fins statistiques et d’analyses.
      </p>
      <h4>7.3 Droit d’accès, de rectification et d’opposition</h4>
      <p>
        Conformément à la réglementation européenne en vigueur, les Utilisateurs
        du site disposent des droits suivants :
      </p>
      <ul>
        <li>
          droit d’accès (article 15 RGPD) et de rectification (article 16 RGPD),
          de mise à jour, de complétude des données des Utilisateurs droit de
          verrouillage ou d’effacement des données des Utilisateurs à caractère
          personnel (article 17 du RGPD), lorsqu’elles sont inexactes,
          incomplètes, équivoques, périmées, ou dont la collecte, l’utilisation,
          la communication ou la conservation est interdite
        </li>
        <li>
          droit de retirer à tout moment un consentement (article 13-2c RGPD)
        </li>
        <li>
          droit à la limitation du traitement des données des Utilisateurs
          (article 18 RGPD)
        </li>
        <li>
          droit d’opposition au traitement des données des Utilisateurs (article
          21 RGPD)
        </li>
        <li>
          droit à la portabilité des données que les Utilisateurs auront
          fournies, lorsque ces données font l’objet de traitements automatisés
          fondés sur leur consentement ou sur un contrat (article 20 RGPD)
        </li>
        <li>
          droit de définir le sort des données des Utilisateurs après leur mort
          et de choisir à qui le responsable du traitement des Données
          Personnelles devra communiquer (ou non) ses données à un tiers qu’il
          aura préalablement désigné
        </li>
      </ul>
      <p>
        Dès que le responsable du traitement des Données Personnelles a
        connaissance du décès d’un Utilisateur et à défaut d’instructions de sa
        part, il s’engage à détruire ses données, sauf si leur conservation
        s’avère nécessaire à des fins probatoires ou pour répondre à une
        obligation légale.
      </p>
      <p>
        Si l’Utilisateur souhaite savoir comment le responsable du traitement
        des Données Personnelles utilise ses Données Personnelles, demande à les
        rectifier ou s’oppose à leur traitement, l’Utilisateur peut le contacter
        par écrit à l’adresse suivante :
      </p>
      <p>ALLIANCE POUR UNE NOUVELLE FRANCE</p>
      <p>58 AVENUE DE WAGRAM – 75017 PARIS – France</p>
      <p>Téléphone : 08 21 61 01 55</p>
      <p>
        Dans ce cas, l’Utilisateur doit indiquer les Données Personnelles qu’il
        souhaiterait que le Délégué à la protection des données corrige, mette à
        jour ou supprime, en s’identifiant précisément avec une copie d’une
        pièce d’identité (carte d’identité ou passeport).
      </p>
      <p>
        Les demandes de suppression de Données Personnelles seront soumises aux
        obligations qui sont imposées au à l'Alliance pour une Nouvelle France
        par la loi, notamment en matière de conservation ou d’archivage des
        documents. Enfin, les Utilisateurs du site peuvent déposer une
        réclamation auprès des autorités de contrôle, et notamment de la CNIL (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.cnil.fr/fr/plaintes"
        >
          https://www.cnil.fr/fr/plaintes
        </a>
        ).
      </p>
      <h4>7.4 Non-communication des données personnelles</h4>
      <p>
        L'Alliance pour une nouvelle France s’interdit de traiter, héberger ou
        transférer les Informations collectées sur ses Utilisateurs vers un pays
        situé en dehors de l’Union Européenne ou reconnu comme &laquo; non
        adéquat &raquo; par la Commission européenne sans en informer
        préalablement l’Utilisateur. Pour autant, il reste libre du choix de ses
        sous- traitants techniques et commerciaux à la condition qu’il
        présentent les garanties suffisantes au regard des exigences du
        Règlement Général sur la Protection des Données (RGPD : n° 2016-679).
      </p>
      <p>
        L'Alliance pour une Nouvelle France s’engage à prendre toutes les
        précautions nécessaires afin de préserver la sécurité des Informations
        et notamment qu’elles ne soient pas communiquées à des personnes non
        autorisées. Cependant, si un incident impactant l’intégrité ou la
        confidentialité des Informations de l’Utilisateur est portée à sa
        connaissance, celui-ci devra dans les meilleurs délais informer
        l’Utilisateur et lui communiquer les mesures de corrections prises. Par
        ailleurs le site ne collecte aucune &laquo; donnée sensible &raquo;.
      </p>
      <p>
        Les Données Personnelles de l’Utilisateur peuvent être traitées par des
        sous-traitants (prestataires de services), exclusivement afin de
        réaliser les finalités de la présente politique.
      </p>
      <p>
        Dans la limite de leurs attributions respectives et pour les finalités
        rappelées ci-dessus, les principales personnes susceptibles d’avoir
        accès aux données des Utilisateurs du site sont principalement les
        agents de notre service éditorial.
      </p>
      <h3>8. Notification d’incident</h3>
      <p>
        Quels que soient les efforts fournis, aucune méthode de transmission sur
        Internet et aucune méthode de stockage électronique n’est complètement
        sûre. Nous ne pouvons en conséquence pas garantir une sécurité absolue.
        Si nous prenions connaissance d’une brèche de la sécurité, nous
        avertirions les utilisateurs concernés afin qu’ils puissent prendre les
        mesures appropriées. Nos procédures de notification d’incident tiennent
        compte de nos obligations légales, qu’elles se situent au niveau
        national ou européen. Nous nous engageons à informer pleinement nos
        utilisateurs de toutes les questions relevant de la sécurité de leur
        compte et à leur fournir toutes les informations nécessaires pour les
        aider à respecter leurs propres obligations réglementaires en matière de
        reporting.
      </p>
      <p>
        Aucune information personnelle de l’utilisateur du site n’est publiée à
        l’insu de l’utilisateur, échangée, transférée, cédée ou vendue sur un
        support quelconque à des tiers.
      </p>
      <h3>Sécurité</h3>
      <p>
        Pour assurer la sécurité et la confidentialité des Données Personnelles
        et des Données Personnelles de Santé, le Site utilise des réseaux
        protégés par des dispositifs standards tels que par pare-feu, la
        pseudonymisation, encryption et identification par mot de passe.
      </p>
      <p>
        Lors du traitement des Données Personnelles, l'Alliance pour une
        Nouvelle France prend toutes les mesures raisonnables visant à les
        protéger contre toute perte, utilisation détournée, accès non autorisé,
        divulgation, altération ou destruction.
      </p>
      <h3>
        9. Liens hypertextes &laquo; cookies &raquo; et balises
        (&ldquo;tags&rdquo;) internet
      </h3>
      <p>
        Le site contient un certain nombre de liens hypertextes vers d’autres
        sites, mis en place avec l’autorisation du site. Cependant, l’Éditeur
        n’a pas la possibilité de vérifier le contenu des sites ainsi visités,
        et n’assumera en conséquence aucune responsabilité de ce fait.
      </p>
      <p>
        Sauf si vous décidez de désactiver les cookies, vous acceptez que le
        site puisse les utiliser. Vous pouvez à tout moment désactiver ces
        cookies et ce gratuitement à partir des possibilités de désactivation
        qui vous sont offertes et rappelées ci-après, sachant que cela peut
        réduire ou empêcher l’accessibilité à tout ou partie des Services
        proposés par le site.
      </p>
      <h4>9.1. &laquo; COOKIES &raquo;</h4>
      <p>
        Un &laquo; cookie &raquo; est un petit fichier d’information envoyé sur
        le navigateur de l’Utilisateur et enregistré au sein du terminal de
        l’Utilisateur (ex : ordinateur, smartphone), (ci-après &laquo; Cookies
        &raquo;). Ce fichier comprend des informations telles que le nom de
        domaine de l’Utilisateur, le fournisseur d’accès Internet de
        l’Utilisateur, le système d’exploitation de l’Utilisateur, ainsi que la
        date et l’heure d’accès. Les Cookies ne risquent en aucun cas
        d’endommager le terminal de l’Utilisateur.
      </p>
      <p>
        L’Editeur est susceptible de traiter les informations de l’Utilisateur
        concernant sa visite du Site, telles que les pages consultées, les
        recherches effectuées. Ces informations permettent à l’éditeur
        d’améliorer le contenu du Site, de la navigation de l’Utilisateur.
      </p>
      <p>
        Les Cookies facilitant la navigation et/ou la fourniture des services
        proposés par le Site, l’Utilisateur peut configurer son navigateur pour
        qu’il lui permette de décider s’il souhaite ou non les accepter de
        manière à ce que des Cookies soient enregistrés dans le terminal ou, au
        contraire, qu’ils soient rejetés, soit systématiquement, soit selon leur
        émetteur. L’Utilisateur peut également configurer son logiciel de
        navigation de manière à ce que l’acceptation ou le refus des Cookies lui
        soient proposés ponctuellement, avant qu’un Cookie soit susceptible
        d’être enregistré dans son terminal. Le Site informe l’Utilisateur que,
        dans ce cas, il se peut que les fonctionnalités de son logiciel de
        navigation ne soient pas toutes disponibles.
      </p>
      <p>
        Si l’Utilisateur refuse l’enregistrement de Cookies dans son terminal ou
        son navigateur, ou si l’Utilisateur supprime ceux qui y sont
        enregistrés, l’Utilisateur est informé que sa navigation et son
        expérience sur le Site peuvent être limitées. Cela pourrait également
        être le cas lorsque l’Éditeur ne peut pas reconnaître, à des fins de
        compatibilité technique, le type de navigateur utilisé par le terminal,
        les paramètres de langue et d’affichage ou le pays depuis lequel le
        terminal semble connecté à Internet.
      </p>
      <p>
        Le cas échéant, L'Alliance pour une Nouvelle France décline toute
        responsabilité pour les conséquences liées au fonctionnement dégradé du
        Site et des services éventuellement proposés par le Site, résultant (i)
        du refus de Cookies par l’Utilisateur (ii) de l’impossibilité pour le
        Site d’enregistrer ou de consulter les Cookies nécessaires à leur
        fonctionnement du fait du choix de l’Utilisateur. Pour la gestion des
        Cookies et des choix de l’Utilisateur, la configuration de chaque
        navigateur est différente. Elle est décrite dans le menu d’aide du
        navigateur, qui permettra de savoir de quelle manière l’Utilisateur peut
        modifier ses souhaits en matière de Cookies.
      </p>
      <p>
        À tout moment, l’Utilisateur peut faire le choix d’exprimer et de
        modifier ses souhaits en matière de Cookies. L’Éditeur pourra en outre
        faire appel aux services de prestataires externes pour l’aider à
        recueillir et traiter les informations décrites dans cette section.
      </p>
      <p>
        Enfin, en cliquant sur les icônes dédiées aux réseaux sociaux Twitter,
        Facebook, Youtube et Instagram figurant sur le Site ou dans son
        application mobile et si l’Utilisateur a accepté le dépôt de cookies en
        poursuivant sa navigation sur le Site Internet ou l’application mobile,
        Twitter, Facebook, Youtube et Instagram peuvent également déposer des
        cookies sur vos terminaux (ordinateur, tablette, téléphone portable).
      </p>
      <p>
        Ces types de cookies ne sont déposés sur vos terminaux qu’à condition
        que vous y consentiez, en continuant votre navigation sur le Site
        Internet ou l’application mobile. À tout moment, l’Utilisateur peut
        néanmoins revenir sur son consentement à ce que le Site dépose ce type
        de cookies.
      </p>
      <h4>Article 9.2. Balises (&ldquo;TAGS&rdquo;) Internet</h4>
      <p>
        Le Site peut employer occasionnellement des balises Internet (également
        appelées &laquo; tags &raquo;, ou balises d’action, GIF à un pixel, GIF
        transparents, GIF invisibles et GIF un à un) et les déployer par
        l’intermédiaire d’un partenaire spécialiste d’analyses Web susceptible
        de se trouver (et donc de stocker les informations correspondantes, y
        compris l’adresse IP de l’Utilisateur) dans un pays étranger.
      </p>
      <p>
        Ces balises sont placées à la fois dans les publicités en ligne
        permettant aux internautes d’accéder au Site, et sur les différentes
        pages de celui-ci.
      </p>
      <p>
        Cette technologie permet à l’Éditeur d’évaluer les réponses des
        visiteurs face au Site et l’efficacité de ses actions (par exemple, le
        nombre de fois où une page est ouverte et les informations consultées),
        ainsi que l’utilisation de ce Site par l’Utilisateur.
      </p>
      <p>
        Le prestataire externe pourra éventuellement recueillir des informations
        sur les visiteurs du Site et d’autres sites Internet grâce à ces
        balises, constituer des rapports sur l’activité du Site à l’attention de
        l’Éditeur, et fournir d’autres services relatifs à l’utilisation de
        celui-ci et d’Internet.
      </p>
      <h3>10. Droit applicable et attribution de juridiction.</h3>
      <p>
        Tout litige en relation avec l’utilisation du site est soumis au droit
        français. En dehors des cas où la loi ne le permet pas, il est fait
        attribution exclusive de juridiction aux tribunaux compétents de Paris.
      </p>
      <h2>Mentions légales relatives aux dons</h2>
      <p>
        Lydie GUARINOS, mandataire financier, est seule habilitée à recueillir
        des dons en faveur de l'ANF.
      </p>
      <p>
        Chaque don fera l’objet d’un reçu fiscal qui vous sera transmis par le
        mandataire financier pour obtenir
        <strong className="strong-left">
          une réduction d’impôt sur le revenu égale à 66 % du montant de votre
          soutien financier
        </strong>
        , dans la limite de 20 % du revenu imposable et de 15 000 € (article 200
        – 1 du Code Général des Impôts). Ce reçu ne comportera pas d’identité du
        bénéficiaire si le don est inférieur ou égal à 3 000 €. Les dons des
        personnes morales (entreprises, associations non politiques) sont
        interdits. Une même personne physique ne peut donner annuellement plus
        de 7 500 € à un ou plusieurs partis politiques.
      </p>
      <h3>Article 11-4 de la loi n° 88-227 de la loi du 11 mars 1988</h3>
      <p>
        Les dons consentis et les cotisations versées en qualité d’adhérent d’un
        ou de plusieurs partis politiques par une personne physique dûment
        identifiée à une ou plusieurs associations agréées en qualité
        d’association de financement ou à un ou plusieurs mandataires financiers
        d’un ou de plusieurs partis politiques ne peuvent annuellement excéder 7
        500 euros. Les personnes morales à l’exception des partis ou groupements
        politiques ne peuvent contribuer au financement des partis ou
        groupements politiques, ni en consentant des dons, sous quelque forme
        que ce soit, à leurs associations de financement ou à leurs mandataires
        financiers, ni en leur fournissant des biens, services ou autres
        avantages directs ou indirects à des prix inférieurs à ceux qui sont
        habituellement pratiqués.
      </p>
      <h3>Article 11-5 de la loi n° 88-227 de la loi du 11 mars 1988</h3>
      <p>
        Ceux qui ont versé des dons à un ou plusieurs partis politiques en
        violation de l’article 11- 4 sont punis d’une amende de 3 750 euros et
        d’un an d’emprisonnement. Les mêmes peines sont applicables au
        bénéficiaire de dons consentis :
      </p>
      <ol>
        <li>
          Par une même personne physique à un seul parti politique en violation
          du premier alinéa du même article 11-4 ;
        </li>
        <li>
          Par une personne morale en violation du troisième alinéa dudit article
          11-4 ;
        </li>
        <li>
          Par un État étranger ou par une personne morale de droit étranger en
          violation du sixième alinéa du même article 11-4.
        </li>
      </ol>
    </div>
  );
};

export default LegalNotices;

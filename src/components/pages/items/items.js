import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import api from "../../../utils/api";

import ListItems from "../../atoms/listItems/listItems";

function Items() {
  const dispatch = useDispatch();

  useEffect(() => {
    const getItems = async () => {
      try {
        dispatch({ type: "ITEMS_FETCH" });
        let items = await api.get("/items");
        dispatch({ type: "ITEMS_FETCH_SUCCESS" });
        dispatch({ type: "ITEMS_SET", payload: items.data.data });
      } catch (err) {
        dispatch({ type: "ITEMS_RESET" });
      }
    };
    getItems();
  }, [dispatch]);

  return (
    <>
      <div className="content" id="program">
        <div className="title">
          <h2>
            NOTRE PROGRAMME POUR LA <span>FRANCE</span>
          </h2>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quibusdam
            recusandae illum, dolorum eos expedita quisquam earum eaque natus
            nesciunt maxime, vero soluta exercitationem consectetur ipsa aperiam
            ad suscipit impedit optio?
          </p>
        </div>
        <div className="box_link_redirectTo">
          <p className="download_program">
            Télécharger notre programme pour la France
          </p>
        </div>
      </div>
      <ListItems />
    </>
  );
}

export default Items;

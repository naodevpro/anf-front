import React from "react";

import FormInformation from "../../molecules/formInformation/formInformation";

import downloadIcon from "./../../../assets/icons/arrow_download_white.svg";
import "./_joinUs.scss";

const JoinUs = () => {
  return (
    <div className="container_responsive">
      <div className="container_join">
        <div className="sub_container">
          <div className="box_text_action mb-2">
            <h1 className="title-1 basic-light-color mt-2">
              Adhérez à l'Alliance pour une Nouvelle France et menez des actions
              concrètes !
            </h1>
            <p className="subtitle basic-light-color mt-1">
              Rendons les français fiers de leur pays. Chaque adhérant est
              important, pour que nous allions ensemble vers une nouvelle
              France.
            </p>
            <p className="download_join">
              <img src={downloadIcon} alt="icon" />
              Vous pouvez également télécharger le bulletin d'adhésion au format
              PDF.
            </p>
          </div>
          <FormInformation />
        </div>
      </div>
    </div>
  );
};
export default JoinUs;

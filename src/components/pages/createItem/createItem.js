import React, { useState, useRef } from "react";
import { Link } from "react-router-dom";

import BreadCrumb from "../../atoms/Breadcrumb/breadCrumb";
import Input from "../../atoms/Input/input";
import Button from "../../atoms/Button/button";

import { handleFormError } from "../../../utils/handleFormError";

import CrossIcon from "../../../assets/icons/cross_icon.png";
import logoWhite from "../../../assets/logos/logo_anf_white.png";

import "./_createItem.scss";

const CreateItem = () => {
  const [steps] = useState([
    {
      title: "Item",
      success: false,
    },
    {
      title: "Constat",
      success: false,
    },
    {
      title: "Objectif",
      success: false,
    },
    {
      title: "Argument",
      success: false,
    },
  ]);

  const [stepFocused] = useState(0);
  const [fields, setFields] = useState([
    {
      id: 0,
      type: "file",
      name: "headerAsset",
      reference: useRef(),
      regexp: `^.{1,}$`,
      errorDisplayed: "Veuillez ajouter une image à l'item",
      error: false,
    },
    {
      id: 1,
      type: "text",
      name: "title",
      reference: useRef(),
      label: "Titre de l'item",
      placeholder: "Titre de l'item",
      regexp: `^.{1,}$`,
      errorDisplayed: "Veuillez entrer un titre",
      error: false,
    },
    {
      id: 2,
      type: "text",
      name: "shortDescription",
      reference: useRef(),
      label: "Petite description",
      placeholder: "Petite description",
      regexp: `^.{1,}$`,
      errorDisplayed: "Veuillez entrer une petite description",
      error: false,
    },
    {
      id: 3,
      type: "text",
      name: "titleLongueDescription",
      reference: useRef(),
      label: "Titre longue description",
      placeholder: "Titre longue description",
      regexp: `^.{1,}$`,
      errorDisplayed: "Veuillez entrer un titre pour la longue description",
      error: false,
    },
    {
      id: 4,
      type: "textarea",
      name: "longueDescription",
      reference: useRef(),
      label: "Longue description",
      placeholder: "Longue description",
      regexp: `^.{1,}$`,
      errorDisplayed: "Veuillez entrer une longue description",
      error: false,
    },
  ]);

  return (
    <div className="container_responsive d-flex justify-content-center">
      <div className="container_additem d-flex flex-column">
        <nav className="p-3">
          <Link to="/">
            <div className="box_logo">
              <img src={logoWhite} alt="logo_white" className="logo_white" />
            </div>
          </Link>
          <div className="box_breadcrumb">
            <BreadCrumb steps={steps} stepFocused={stepFocused} />
          </div>
        </nav>
        <div className="sub_container">
          <div className="box_text_action mb-2">
            <h1 className="title-1 basic-light-color mt-2">
              Parce que chaque thème est important pour une nouvelle France !
            </h1>
            <p className="subtitle basic-light-color mt-1">
              Grâce à cette interface, il vous est facile de gérer les items
              présent sur le site-web ! N'hésitez pas à faire remonter à
              l'équipe technique le moindre problème rencontrés afin que ceux-là
              soit mis à jour au plus vite !
            </p>
          </div>
          <div className="box_form_additem p-1">
            <form
              className="basic-light-background p-1 mb-3"
              autoComplete="off"
            >
              {fields
                ? fields.map((field, index) => {
                    return (
                      <div className="d-flex flex-column" key={index}>
                        {(() => {
                          if (field.type === "file") {
                            return (
                              <div className="box_input_label">
                                <label className="input_file_label mb-1">
                                  <p>+</p>
                                  <Input
                                    customClass={"input_file mb-1"}
                                    type={"file"}
                                    id={field.id}
                                    name={field.name}
                                    onChangeMethod={handleFormError}
                                    onChangeParams={[
                                      fields,
                                      field,
                                      setFields,
                                      index,
                                    ]}
                                  />
                                </label>
                              </div>
                            );
                          } else if (field.type === "textarea") {
                            return (
                              <div>
                                <label className="label" htmlFor={field.id}>
                                  {field.label}
                                </label>
                                <textarea
                                  ref={field.reference}
                                  id={field.id}
                                  name={field.name}
                                  className="mb-1"
                                  type={field.type}
                                  placeholder={field.placeholder}
                                  style={
                                    field.error
                                      ? { backgroundImage: `url(${CrossIcon}` }
                                      : null
                                  }
                                  onChange={(e) =>
                                    handleFormError(
                                      e,
                                      fields,
                                      field,
                                      setFields,
                                      index
                                    )
                                  }
                                />
                              </div>
                            );
                          } else {
                            return (
                              <Input
                                reference={field.reference}
                                id={field.id}
                                name={field.name}
                                customClass={"input mb-1 input--icon-right"}
                                height={"45px"}
                                width={"100%"}
                                type={field.type}
                                label={field.label}
                                placeholder={field.placeholder}
                                onChangeMethod={handleFormError}
                                onChangeParams={[
                                  fields,
                                  field,
                                  setFields,
                                  index,
                                ]}
                                icon={field.error ? CrossIcon : null}
                              />
                            );
                          }
                        })()}
                        {field.error ? (
                          <div className="box box-error">
                            <div className="up-triangle up-triangle-error"></div>
                            <span>{field.errorDisplayed}</span>
                          </div>
                        ) : null}
                      </div>
                    );
                  })
                : null}
              <Button
                customClass={
                  "btn btn--squared primary-background basic-light-color"
                }
                label={"suivant"}
                height={"50px"}
                width={"100%"}
              />
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreateItem;

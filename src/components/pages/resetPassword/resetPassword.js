import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import api from "./../../../utils/api";

import renderNotification from "../../../utils/notification";

import Button from "./../../atoms/Button/button"

import Icon from "../../../assets/icons/arrow_right.svg"
import "./_resetPassword.scss";


const ResetPassword = () => {
  const history = useHistory();
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [formValideClass, setFormValideClass] = useState("");
  const [isDisabled, setDisabled] = useState(true);
  const [open, setOpen] = useState(false);
  const [forgetSuccess, setForgetSuccess] = useState(null);
  const [notif, setNotif] = useState(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const result = await api.post("/reset/password", {
        password: password,
      });
      setNotif(result.data.data);
      setOpen(true);
      history.push({
        pathname: "/login",
      });
      return setForgetSuccess(true);
    } catch (err) {
      setNotif(
        err.response.data.data
          ? err.response.data.data
          : err.response.data.message.message
      );
      setOpen(true);
      return setForgetSuccess(false);
    }
  };

  const handleChange = (e) => {
    const regex = new RegExp(
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/
    );
    let name = e.target.name;
    let value = e.target.value;

    if (value === "" || !value) {
      e.target.className = "initial";
      setDisabled(true);
    } else {
      if (regex.test(value)) {
        if (name === "password") {
          if (!passwordConfirm || "") {
            e.target.className = "valide";
            setDisabled(true);
          } else {
            if (passwordConfirm === value) {
              setFormValideClass("valide");
              setDisabled(false);
            } else {
              e.target.className = "error";
              setDisabled(true);
            }
          }
        } else {
          if (password === value) {
            e.target.className = "valide";
            setDisabled(false);
          } else {
            e.target.className = "error";
            setDisabled(true);
          }
        }
      } else {
        e.target.className = "error";
        setDisabled(true);
      }
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      {open && forgetSuccess === true
        ? renderNotification(200, "Demande réussie", notif, handleClose)
        : null}
      {open && forgetSuccess === false
        ? renderNotification(400, "Erreur", notif, handleClose)
        : null}
      <div className="container_reset_password">
        <div className="box_reset_password">
          <form onSubmit={handleSubmit} autoComplete="off">
            <h1>
              Récupération de <span>votre mot de passe</span>
            </h1>
            <p>Renseignez votre mot de passe dans les champs ci-dessous</p>
            <input
              className={formValideClass || "initial"}
              type="password"
              name="password"
              placeholder="Votre nouveau mot de passe"
              onChange={(e) => {
                setPassword(e.target.value);
                handleChange(e);
              }}
            ></input>
            <input
              className={formValideClass || "initial"}
              type="password"
              name="passwordConfirm"
              placeholder="Retapez votre nouveau mot de passe"
              onChange={(e) => {
                setPasswordConfirm(e.target.value);
                handleChange(e);
              }}
            ></input>
            <Button
              label={"Suivant"}
              height={"70px"}
              width={"100%"}
              type="submit"
              icon={Icon}
              isDisabled={isDisabled}
              customClass={
                "btn basic-light-color primary-background"
              }
              onClickMethod={handleSubmit}
            />
          </form>
        </div>
      </div>
    </>
  );
};

export default ResetPassword;

import React, { useState, useRef } from "react";
import { useHistory } from "react-router";

import api from "../../../utils/api";
import renderNotification from "../../../utils/notification";
import { toggleClass, addClass, removeClass } from "../../../utils/changeClass";

import Button from "../../atoms/Button/button";
import Input from "../../atoms/Input/input";

import "./_forgetPassword.scss";
import Icon from "../../../assets/icons/arrow_right.svg";
import Check from "../../../assets/icons/green_check.svg";

const ForgetPassword = () => {
  let history = useHistory();
  const btnSend = useRef();
  const inputSend = useRef();
  const [isDisabled, setIsDisabled] = useState(true);
  const [email, setEmail] = useState("");
  const [open, setOpen] = useState(false);
  const [forgetSuccess, setForgetSuccess] = useState(false);
  const [notif, setNotif] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const result = await api.post("/forget/password", {
        email: email,
      });
      setNotif(result.data.data);
      setOpen(true);
      return setForgetSuccess(true);
    } catch (err) {
      setNotif(
        err.response.data.data
          ? err.response.data.data
          : err.response.data.message.message
      );
      setOpen(true);
      return setForgetSuccess(false);
    }
  };

  const handleChange = () => {
    const regex = new RegExp(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    if (!regex.test(inputSend.current.value)) {
      setIsDisabled(true);
      toggleClass(inputSend, "error-border", "success-border");
      addClass(btnSend, "disabled-background");
    } else {
      setIsDisabled(false);
      toggleClass(inputSend, "success-border", "error-border");
      removeClass(btnSend, "disabled-background");
      setEmail(inputSend.current.value);
    }
  };

  const redirectToLoginPage = () => {
    history.push("/login");
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      {open && forgetSuccess === true
        ? renderNotification(200, "Demande réussie", notif, handleClose)
        : null}
      {open && forgetSuccess === false
        ? renderNotification(400, "Erreur", notif, handleClose)
        : null}
      <div className="container_forgetPassword">
        <div className="box_forgetPassword">
          <form className="m-5">
            {!forgetSuccess ? (
              <>
                <h1>
                  Votre <span>adresse email</span>
                </h1>
                <p>
                  Renseignez votre adresse email, vous recevrez un lien pour
                  récuperer votre mot de passe.
                </p>
                <Input
                  reference={inputSend}
                  type={"text"}
                  id={"email"}
                  name={"email"}
                  placeholder={"Tapez votre adresse e-mail"}
                  width={"100%"}
                  height={"50px"}
                  customClass={"input"}
                  onChangeMethod={handleChange}
                />
                <div className="box_forgetPassword_btns">
                  <Button
                    reference={btnSend}
                    label={"Envoyer"}
                    height={"70px"}
                    width={"100%"}
                    id="send_forget"
                    type="submit"
                    icon={Icon}
                    customClass={"btn basic-light-color primary-background"}
                    isDisabled={isDisabled}
                    onClickMethod={handleSubmit}
                  />
                </div>
              </>
            ) : (
              <>
                <h1 className="title_success">
                  Récupération de votre mot de passe
                </h1>
                <p className="notif">
                  <img src={Check} alt="logo_check" className="logo_check" />
                  {notif} : <span className="email_success">{email}</span>
                </p>
                <p className="text_reset">
                  Veuillez suivre les étapes de récupération de votre mot de
                  passe indiquées dans
                  <br /> l'e-mail puis vous connecter avec votre nouveau mot de
                  passe.
                </p>

                <div className="box_forgetPassword_btns">
                  <Button
                    reference={btnSend}
                    label={"RETOUR A LA PAGE DE CONNEXION"}
                    height={"70px"}
                    width={"100%"}
                    id="send_forget"
                    type="submit"
                    icon={Icon}
                    customClass={"btn basic-light-color primary-background"}
                    isDisabled={isDisabled}
                    onClickMethod={redirectToLoginPage}
                  />
                </div>
              </>
            )}
          </form>
        </div>
      </div>
    </>
  );
};

export default ForgetPassword;

import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import api from "../../../utils/api";

import squareRed from "../../../assets/icons/square_red.svg";
import squareBlue from "../../../assets/icons/square_blue.svg";
import arrow from "../../../assets/illustrations/arrow.png";

import "./_item.scss";

function Item() {
  const appState = useSelector((state) => state.item.item.values);
  const dispatch = useDispatch();
  const { id } = useParams();

  useEffect(() => {
    const appInit = async () => {
      const getItemById = async () => {
        try {
          dispatch({ type: "ITEM_FETCH" });
          let items = await api.get(`/items/${id}`);
          dispatch({ type: "ITEM_FETCH_SUCCESS" });
          dispatch({ type: "ITEM_SET", payload: items.data.data });
        } catch (err) {
          dispatch({ type: "ITEM_RESET" });
        }
      };

      getItemById();
    };
    appInit();
  }, [dispatch, id]);

  return (
    <>
      {appState ? (
        <div className="container_global">
          <div className="container_title_description">
            <h1 className="title">
              Le programme de l'Alliance vers une Nouvelle France pour
              <span> {appState.title}</span>
            </h1>
            <span className="shortDescription">
              {appState.shortDescription}
            </span>
          </div>

          <div
            className="bg_image"
            style={{
              backgroundImage: `url(${appState.headerAsset})`,
            }}
          ></div>

          <div className="box_description">
            <h2 className="titleLongDescription">
              {appState.titleLongDescription}
            </h2>
            <p className="longDescription">{appState.longDescription}</p>
          </div>

          <div className="box_constat">
            {appState
              ? appState.constats.map((constat, index) => {
                  return (
                    <>
                      <h3 className="titleConstat" key={index}>
                        {constat.titleConstat}
                      </h3>
                      <div className="container_subjectConstat">
                        <div className="subjectConstat">
                          <div className="red_square">
                            <img src={squareRed} alt="square_red" />
                          </div>
                          <div className="container_title_content">
                            <h4 className="titleSubjectConstat">
                              {constat.titleConstat}
                            </h4>
                            <div
                              className="container_img"
                              style={{
                                backgroundImage: `url("${constat.assetConstat}")`,
                              }}
                            >
                              <div className="img"></div>
                            </div>
                            <p className="contentSubjectConstat">
                              {constat.contentConstat}
                            </p>
                          </div>
                        </div>
                      </div>
                    </>
                  );
                })
              : null}
          </div>

          <div className="box_proposition">
            {appState
              ? appState.objectifs.map((objectif, index) => {
                  return (
                    <div className="container_objectifs" key={index}>
                      <div className="objectifs">
                        <div className="blue_square">
                          <img src={squareBlue} alt="square_blue" />
                        </div>
                        <div className="container_title_content">
                          <h4 className="titleObjectif">
                            {objectif.titleObjectif}
                          </h4>
                          <p className="descriptionObjectif">
                            {objectif.descriptionObjectif}
                          </p>
                        </div>
                      </div>
                    </div>
                  );
                })
              : null}
            {appState
              ? appState.objectifs.map((objectif) => {
                  return objectif.arguments.map((argument, index) => {
                    return (
                      <div className="container_arguments" key={index}>
                        <div className="arguments">
                          <div className="arrow">
                            <img src={arrow} alt="arrow" />
                          </div>
                          <div className="container_title_content">
                            <h4 className="titleArgument">
                              {argument.titleArgument}
                            </h4>
                            <p className="descriptionArgument">
                              {argument.descriptionArgument}
                            </p>
                          </div>
                        </div>
                      </div>
                    );
                  });
                })
              : null}

            <div className="container_citation">
              <h4 className="citation">{appState.citation}</h4>
              <p className="author">Philippe JOME - Membre du parti de l'ANF</p>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
}

export default Item;

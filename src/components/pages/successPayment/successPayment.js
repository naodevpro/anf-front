import React from "react";
import { Link, useLocation } from "react-router-dom";


import "./_successPayment.scss";

import Button from "../../atoms/Button/button";

function SuccessPayment() {
  let location = useLocation();

  return (
    <div className="container_success">
      <div className="box_title_paragraph">
        <h1>
          Merci {location.state.firstname} {location.state.lastname} pour votre
          don !
        </h1>
        <p>
          Rendons les français fiers de leur pays. Chaque don compte et sera
          prit en compte, avec la mise en place d'actions concrètes pour la
          France.
        </p>
        <Link to="/">
          <Button
            label={"Suivant"}
            height={"70px"}
            width={"550px"}
            id="send_forget"
            type="submit"
            customClass={
              "btn basic-light-color primary-background"
            }
          />
        </Link>
      </div>
    </div>
  );
}

export default SuccessPayment;

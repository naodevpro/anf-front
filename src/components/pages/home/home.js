import React from "react";

import Header from "../../organisms/header/header";
import WordsAction from "../../molecules/wordsAction/wordsAction";
import OurPriorities from "../../molecules/ourPriorities/ourPriorities";
import WhyJoinUs from "../../molecules/whyJoinUs/whyJoinUs";
import OurVision from "../../molecules/ourVision/ourVision";
import DonationsBar from "../../molecules/donationsBar/donationsBar";
import OurProgram from "../../molecules/ourProgram/ourProgram";
import ContactUs from "../../molecules/contactUs/contactUs";
import HowInvolved from "../../molecules/howInvolved/howInvolved";

function Home() {
  return (
    <>
      <Header />
      <WordsAction />
      <OurPriorities />
      <WhyJoinUs />
      <OurVision />
      <DonationsBar />
      <OurProgram />
      <ContactUs />
      <HowInvolved />
    </>
  );
}

export default Home;

import React, { useState, useEffect, useCallback } from "react";
import { Link, useParams, useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";

import BoxNotification from "../../atoms/boxNotification/boxNotification";
import Button from "../../atoms/Button/button";
import Input from "../../atoms/Input/input";

import "./_payment.scss";
import logoWhite from "../../../assets/logos/logo_anf_white.png";

import {
  useStripe,
  useElements,
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
} from "@stripe/react-stripe-js";

import api from "../../../utils/api";
import fieldsList from "./field.json";

const Payment = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const stripe = useStripe();
  const elements = useElements();
  const [open, setOpen] = useState(false);
  const [fields, setFields] = useState(fieldsList["Donor"]);
  const [stripeFields, setStripeFields] = useState(fieldsList["Stripe"]);
  const [mount, setMount] = useState(false);


  const [validateForm, setValidateForm] = useState({
    valide: false,
  });
  const [paymentSuccess, setPaymentSuccess] = useState(false);
  const [isStripeFieldComplete, setIsStripeFieldComplete] = useState(false);

  const amount = useParams().amount;

  const initiateStripeFields = useCallback(() => {
    const stripeFieldsClone = [...stripeFields].map((field) => {
      if (field.fieldName === "amount") field.value = amount;
      return field;
    });
    setStripeFields(stripeFieldsClone);
  }, [amount, stripeFields]);

  useEffect(() => {
    if (!mount) {
      setMount(true);
      initiateStripeFields();
    } else {
      return null;
    }
  }, [initiateStripeFields, mount]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    scrollToTop();

    let formIsValide = validationHandlerSubmit();

    if (formIsValide) {
      if (!stripe || !elements) {
        return;
      }
      const CardNumberEl = elements.getElement(CardNumberElement);
      const { error } = await stripe.createPaymentMethod({
        type: "card",
        card: CardNumberEl,
      });

      if (error) {
        setOpen(true);
        setIsStripeFieldComplete(false);
      } else {
        try {
          const token = await stripe.createToken(CardNumberEl);
          const body = setBodyFields(fields);
          body.token = token.token.id;
          body.amount = stripeFields[0].value;
          const donation = await api.post("/donations", body);
          dispatch({
            type: "PAYMENT_SET",
            payload: donation.data.data,
          });
          setOpen(true);
          setPaymentSuccess(true);
          history.push({
            pathname: "/success/payment",
            state: {
              firstname: body.firstname,
              lastname: body.lastname,
            },
          });
        } catch (error) {
          dispatch({
            type: "PAYMENT_ERROR",
            payload: error,
          });
          setOpen(true);
          setPaymentSuccess(false);
        }
      }
    } else {
      setOpen(true);
      return setValidateForm({ valide: false });
    }
  };

  const setBodyFields = (fields) => {
    let body = {};
    fields.forEach((field) => {
      body[field.fieldName] = field.value;
    });

    return body;
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  const renderNotificationBox = (
    code,
    titleNotification,
    descriptionNotification
  ) => {
    return (
      <BoxNotification
        code={code}
        titleNotification={titleNotification}
        descriptionNotification={descriptionNotification}
        handleClose={handleClose}
      />
    );
  };

  const validationHandler = (behavior) => {
    let formHasError = false;
    const fieldsErrors = fields.map((field) => {
      let error = fieldValidation(field);
      if (error) {
        formHasError = true;
        field.valide = false;
        field.error = error;
        if (behavior === "submit") field.inital = false;
      } else {
        field.valide = true;
        field.error = null;
      }
      return field;
    });

    if (behavior === "submit") {
      return {
        fields: fieldsErrors,
        isFormContainError: formHasError,
      };
    }
  };

  const validationHandlerSubmit = () => {
    setValidateForm({
      valide: false,
    });
    let fieldsValidation = validationHandler("submit");
    let fieldsValidationStripe = validationHandlerStripe("submit");
    let isFormContainError = fieldsValidation.isFormContainError;
    let isFormContainErrorStripe = fieldsValidationStripe.isFormContainError;
    setFields(fieldsValidation.fields);
    setStripeFields(fieldsValidationStripe.fields);

    if (isFormContainError || isFormContainErrorStripe) {
      setOpen(true);
      setValidateForm({
        valide: false,
      });
      return false;
    } else {
      setValidateForm({
        valide: true,
      });

      return true;
    }
  };

  const validationHandlerStripe = (behavior) => {
    let formHasError = false;
    const fieldsErrors = stripeFields.map((field) => {
      let error = fieldValidation(field);
      if (error) {
        formHasError = true;
        field.valide = false;
        field.error = error;
        if (behavior === "submit") field.inital = false;
      } else {
        field.valide = true;
        field.error = null;
      }

      return field;
    });

    if (behavior === "submit") {
      return {
        fields: fieldsErrors,
        isFormContainError: formHasError,
      };
    }
  };

  const fieldValidation = (field) => {
    let error = null;

    if (!field.allowNull) {
      if (!isInputEmpty(field.value)) {
        error = true;
        return error;
      }
    }
    if (field.validationOptions.length !== null) {
      if (
        !lengthValidation(
          field.value,
          field.validationOptions.length.min,
          field.validationOptions.length.max,
          field.validationOptions.inputType
        )
      ) {
        error = true;
        return error;
      }
    }

    if (field.validationOptions.inputType === "tel") {
      if (!telTypeValidation(field.value)) {
        error = true;
        return error;
      }
    }

    if (field.validationOptions.inputType === "email") {
      if (!emailTypeValidation(field.value)) {
        error = true;
        return error;
      }
    }
    return null;
  };

  const isInputEmpty = (value) => {
    if (value === "" || !value) return false;
    else return true;
  };

  function isNumeric(num) {
    return !isNaN(num);
  }

  const lengthValidation = (value, min, max, inputType) => {
    if (inputType === "number") {
      if (isNumeric(value)) {
        let nb = parseInt(value);
        if (nb < min || nb > max) return false;
      } else return false;
    }
    if (value.length < min || value.length > max) return false;
    else return true;
  };

  const emailTypeValidation = (value) => {
    const regex = new RegExp(/^[a-zA-Z0-9-.]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/);
    return regex.test(value);
  };

  const telTypeValidation = (value) => {
    const regex = new RegExp(`(0|\\+33|0033)[1-9][0-9]{8}`);
    return regex.test(value);
  };

  const handleChange = (e) => {
    const fieldsClone = [...fields].map((field, i) => {
      if (e.target.name === field.labelField) {
        field.value = e.target.value;
        if (field.inital) field.inital = false;
        field.valide = true;
      }

      return field;
    });
    setFields(fieldsClone);
    validationHandler();
  };

  const handleChangeStripe = (e) => {
    const fieldsClone = [...stripeFields].map((field, i) => {
      if (e.target.name === field.labelField) {
        if (field.validationOptions.inputType === "number") {
          if (e.target.value.length <= field.validationOptions.length.max_num)
            field.value = e.target.value.replace(",", "");
        } else field.value = e.target.value;
        if (field.inital) field.inital = false;
        field.valide = true;
      }

      return field;
    });
    setStripeFields(fieldsClone);
    validationHandlerStripe();
  };

  const radioField_render = (field, className, index) => {
    let inputName = field.labelField;
    return (
      <div className="inputs " key={index}>
        <div className="radio-list">
          {field.options.map((value, i) => {
            return (
              <div key={i}>
                <label>{value}</label>
                <input
                  type="radio"
                  name={inputName}
                  value={value}
                  className={className.replace("input", "radio")}
                  onChange={handleChange}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  const formatNumber_to_devise = (value) => {
    if (value !== "")
      return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    else return value;
  };

  const numberField_render = (field, className, index) => {
    let inputName = field.labelField;
    let textNumber_value = formatNumber_to_devise(field.value);

    return (
      <div className="inputs" key={index}>
        <label htmlFor="">
          {field.labelField} {!field.allowNull && "*"}
        </label>
        <Input
          name={inputName}
          width={'100%'}
          type="text"
          data-value-unformatted-number={field.value}
          placeholder={field.placeholder}
          value={textNumber_value}
          customClass="input"
          onChangeMethod={handleChangeStripe}
        />
        {/* <input
          type="text"
          name={inputName}
          data-value-unformatted-number={field.value}
          placeholder={field.placeholder}
          value={textNumber_value}
          className={className}
          onChange={handleChangeStripe}
        /> */}
      </div>
    );
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      {open && !validateForm.valide
        ? renderNotificationBox(
          400,
          "Erreur",
          "Certains champs ne sont pas valide, veuillez à valider tous les champs en surbrillance rouge ❌"
        )
        : null}
      {open && !paymentSuccess && validateForm.valide
        ? renderNotificationBox(
          400,
          "Erreur",
          "Il semble y avoir un problème lors du paiement  ! ❌"
        )
        : null}
      {open && !isStripeFieldComplete && validateForm.valide
        ? renderNotificationBox(
          400,
          "Erreur",
          "Veuillez remplir des coordonées bancaires valides ! ❌"
        )
        : null}
      {open && paymentSuccess
        ? renderNotificationBox(
          200,
          "Succès",
          "Le paiement à bien été effectué, Merci pour votre donation ! ✅"
        )
        : null}
      <div className="container_payment">
        <div className="background_img">
          <Link to="/">
            <div className="box_logo">
              <img src={logoWhite} alt="logo_white" className="logo_white" />
            </div>
          </Link>

          <div className="box_form">
            <div className="box_title_paragraph">
              <h1>
                Faites un don et mener des actions concrètes pour la France !
              </h1>
              <p>
                Rendons les français fiers de leur pays. Chaque don compte et
                sera prit en compte, avec la mise en place d'actions concrètes
                pour la France.
              </p>
            </div>
            <form
              className="form_payment"
              onSubmit={handleSubmit}
              autoComplete="off"
            >
              {fields.map((field, index) => {
                let className = "one-input";
                if (field.value === "") {
                  if (!field.inital) className = "one-input-error";
                } else if (field.valide) {
                  className = "one-input-success";
                } else className = "one-input-error";
                if (field.validationOptions.inputType === "radio")
                  return radioField_render(field, className, index);
                else
                  return (
                    <div className="inputs" key={index}>
                      <label htmlFor="">
                        {field.labelField} {!field.allowNull && "*"}
                      </label>
                      {/* <input
                        type={
                          field.validationOptions.inputType === "number"
                            ? "text"
                            : field.validationOptions.inputType
                        }
                        name={field.labelField}
                        id=""
                        placeholder={field.placeholder}
                        defaultValue={field.value}
                        className={className}
                        onChange={handleChange}
                      /> */}
                      <Input
                        name={field.labelField}
                        type={
                          field.validationOptions.inputType === "number"
                            ? "text"
                            : field.validationOptions.inputType
                        }
                        defaultValue={field.value}
                        placeholder={field.placeholder}
                        width={'100%'}
                        customClass={className}
                        onChangeMethod={handleChange}
                      />
                    </div>
                  );
              })}
              <hr className="form-separator"></hr>
              {!stripe ? (
                <p>Chargement module STRIPE ...</p>
              ) : (
                <>
                  {stripeFields.map((field, index) => {
                    let className = "one-input";
                    if (field.value === "") {
                      if (!field.inital) className = "one-input-error";
                    } else if (field.valide) {
                      className = "one-input-success";
                    } else className = "one-input-error";
                    let textNumber_value = null;
                    if (field.validationOptions.inputType === "number") {
                      textNumber_value = formatNumber_to_devise(field.value);
                    }
                    if (field.validationOptions.inputType === "radio")
                      return radioField_render(field, className, index);
                    else if (field.validationOptions.inputType === "number")
                      return numberField_render(field, className, index);
                    else
                      return (
                        <div className="inputs" key={index}>
                          <label htmlFor="">
                            {field.labelField} {!field.allowNull && "*"}
                          </label>
                          {/* <input
                            type={
                              field.validationOptions.inputType === "number"
                                ? "text"
                                : field.validationOptions.inputType
                            }
                            name={field.labelField}
                            id=""
                            placeholder={field.placeholder}
                            defaultValue={field.value}
                            value={textNumber_value}
                            className={className}
                            onChange={handleChangeStripe}
                          /> */}
                          <Input
                            name={field.labelField}
                            type={
                              field.validationOptions.inputType === "number"
                                ? "text"
                                : field.validationOptions.inputType
                            }
                            placeholder={field.placeholder}
                            width={'100%'}
                            customClass="input"
                            value={textNumber_value}
                            onChangeMethod={handleChangeStripe}
                          />
                        </div>
                      );
                  })}
                  <div className="inputs">
                    <label htmlFor="">Numéro de carte</label>
                    <CardNumberElement className="card-element" required />
                  </div>
                  <div className="inputs">
                    <label htmlFor="">Date d’expiration</label>
                    <CardExpiryElement className="card-element" />
                  </div>
                  <div className="inputs">
                    <label htmlFor="">Cryptogramme</label>
                    <CardCvcElement className="card-element" />
                  </div>
                </>
              )}
              <div className="certif-checkbox">
                <div>
                  {/* <input type="checkbox" required></input> */}
                  <Input type="checkbox" required />

                </div>
                <p>
                  Je certifie sur l'honneur que je suis{" "}
                  <span>une personne physique</span> et que le règlement de mon
                  don ne provient pas du compte d'une personne morale (société,
                  association, collectivité...) :{" "}
                  <span>
                    que le paiement de mon don provient de mon compte bancaire
                    personnel ou de celui de mon conjoint, concubin, ascendant
                    ou descendant.
                  </span>
                </p>
              </div>
              <div className="box_button_give">
                <Button
                  label={"Je donne maintenant"}
                  height={"70px"}
                  width={"100%"}
                  type="submit"
                  disabled={!stripe}
                  customClass={"btn basic-light-color primary-background"}
                  onClickMethod={scrollToTop}
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Payment;

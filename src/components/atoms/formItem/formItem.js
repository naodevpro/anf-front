import React, { useRef } from "react";
import Button from "../../atoms/Button/button";
import Input from "../../atoms/Input/input";

const FormItem = ({ form, item, setItem, submit, scrollToTop }) => {
  const inputHeaderAsset = useRef({ value: null });
  const inputTitle = useRef({ value: null });
  const inputShortDescription = useRef({ value: null });
  const inputTitleLongDescription = useRef({ value: null });

  return (
    <form
      className="item_form"
      ref={form}
      onSubmit={(e) => submit(e, "/items")}
    >
      <div className="form_text_center">
        <label className="input_file_label">
          <p>+</p>
          <Input
            customClass={"input_file"}
            reference={inputHeaderAsset}
            type={"file"}
            id={"headerAsset"}
            name={"headerAsset"}
            onChangeMethod={setItem}
            onChangeParams={[
              { ...item },
              { headerAsset: inputHeaderAsset.current.value },
            ]}
          />
        </label>
      </div>
      <div className="form_text_left">
        <Input
          reference={inputTitle}
          customClass={"input"}
          label={"Titre de l'item"}
          type={"text"}
          id={"title"}
          name={"title"}
          placeholder={"Titre de l'item"}
          height={"50px"}
          width={"100%"}
          onChangeMethod={setItem}
          onChangeParams={[{ ...item }, { title: inputTitle.current.value }]}
        />
        <Input
          reference={inputShortDescription}
          customClass={"input"}
          label={"Petite description"}
          id={"shortDescription"}
          name={"shortDescription"}
          type={"text"}
          placeholder={"Petite description"}
          height={"50px"}
          width={"100%"}
          onChangeMethod={setItem}
          onChangeParams={[
            { ...item },
            { shortDescription: inputShortDescription.current.value },
          ]}
        />
        <Input
          reference={inputTitleLongDescription}
          customClass={"input"}
          label={"Titre longue description"}
          id={"titleLongDescription"}
          name={"titleLongDescription"}
          type={"text"}
          placeholder={"Titre de longue description"}
          height={"50px"}
          width={"100%"}
          onChangeMethod={setItem}
          onChangeParams={[
            { ...item },
            { titleLongDescription: inputTitleLongDescription.current.value },
          ]}
        />
        <label htmlFor="longDescription">Longue description</label>
        <textarea
          id="longDescription"
          type="text"
          name="longDescription"
          placeholder="Longue description"
          onChange={(e) =>
            setItem({ ...item, longDescription: e.target.value })
          }
        />
      </div>
      <Button
        label={"suivant"}
        height={"40px"}
        width={"100%"}
        customClass={"btn basic-light-color primary-background"}
        type="submit"
        onClickMethod={scrollToTop}
      />
    </form>
  );
};

export default FormItem;

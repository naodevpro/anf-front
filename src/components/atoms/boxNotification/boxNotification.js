import React, { useState, useEffect } from "react";

import "./_boxNotification.scss";

import crossRed from "./icons/cross-red.svg";
import validate from "./icons/validate.svg";
import warning from "./icons/warning.svg";
import infos from "./icons/infos.svg";

const BoxNotification = ({
  code,
  titleNotification,
  descriptionNotification,
  handleClose,
}) => {
  const [iconNotification, setIconNotification] = useState("");
  const [colorNotification, setColorNotification] = useState("");

  setTimeout(() => {
    handleClose();
  }, 3000);

  useEffect(() => {
    switch (code) {
      case 400:
        setColorNotification("red");
        return setIconNotification(crossRed);
      case 200:
        setColorNotification("#3ABA70");
        return setIconNotification(validate);
      case 300:
        setColorNotification("#FECD31");
        return setIconNotification(warning);
      case 100:
        setColorNotification("#5F7EEB");
        return setIconNotification(infos);
      default:
        break;
    }
  }, [code]);

  return (
    <div className="container_notification">
      <div className="box_notification">
        <div
          className="color"
          style={{ backgroundColor: colorNotification }}
        ></div>
        <div className="container_cross">
          <img src={iconNotification} alt="logo_cross" />
        </div>
        <div className="container_msg">
          <h2>{titleNotification ? titleNotification : null}</h2>
          <p>{descriptionNotification}</p>
        </div>
        <div className="container_close" onClick={handleClose}>
          <span className="title_close">FERMER</span>
        </div>
      </div>
    </div>
  );
};

export default BoxNotification;

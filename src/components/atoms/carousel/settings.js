const settings = {
  dots: true,
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: false,
  speed: 700,
  swipe: false,
  cssEase: "linear",
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        swipe: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        swipe: true,
      },
    },
    {
      breakpoint: 558,
      settings: {
        slidesToShow: 1,
        swipe: true,
      },
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        swipe: true,
      },
    },
    {
      breakpoint: 300,
      settings: {
        slidesToShow: 1,
        swipe: true,
      },
    },
  ],
};

export default settings;

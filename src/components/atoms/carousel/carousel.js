import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import Slider from "react-slick";

import settings from "./settings";

import "./_carousel.scss";

const Carousel = () => {
  const appState = useSelector((state) => state.items.items.values);

  return (
    <>
      <div className="container_global_responsive">
        <div className="carousel">
          <Slider {...settings}>
            {appState
              ? appState.map((item, index) => {
                  return (
                    <div key={index}>
                      <div className="items">
                        <Link to={`/item/${item.id}`}>
                          <div
                            className="container_images"
                            style={{
                              backgroundImage: `url(${item.headerAsset})`,
                            }}
                          >
                            <h3 className="title_item">
                              {item.title.toUpperCase()}
                            </h3>
                          </div>
                        </Link>
                      </div>
                    </div>
                  );
                })
              : null}
          </Slider>
        </div>
      </div>
    </>
  );
};

export default Carousel;

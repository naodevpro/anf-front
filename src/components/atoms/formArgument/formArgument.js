import React, { useRef } from "react";
import Button from "../../atoms/Button/button";
import Input from "../../atoms/Input/input";

const FormArgument = ({
  argument,
  setArgument,
  objectifId,
  submit,
  scrollToTop,
}) => {
  const inputTitleArgument = useRef();

  const handleSetArgument = () => {
    setArgument({
      ...argument,
      titleArgument: inputTitleArgument.current.value,
    });
  };

  return (
    <form
      className="argument_form"
      onSubmit={(e) => submit(e, "/arguments", argument, objectifId)}
    >
      <div className="form_text_left">
        <Input
          reference={inputTitleArgument}
          customClass={"input"}
          label={"Titre de l'argument"}
          type={"text"}
          id={"titleArgument"}
          name={"titleArgument"}
          placeholder={"Titre de l'argument"}
          height={"50px"}
          width={"100%"}
          onChangeMethod={handleSetArgument}
        />
        <label htmlFor="descriptionArgument">Description de l'argument</label>
        <textarea
          id="descriptionArgument"
          type="text"
          name="descriptionArgument"
          placeholder="Description de l'argument"
          onChange={(e) =>
            setArgument({ ...argument, descriptionArgument: e.target.value })
          }
        />
      </div>
      <Button
        label={"Terminer"}
        height={"40px"}
        width={"100%"}
        type="submit"
        customClass={"btn basic-light-color primary-background"}
        onClickMethod={scrollToTop}
      />
    </form>
  );
};

export default FormArgument;

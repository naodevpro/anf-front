import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import Modal from "../modal/modal";

import "./_listItems.scss";

import Add from "../../atoms/svgr/Add";
import trash from "../../../assets/icons/trash.svg";
import edit from "../../../assets/icons/edit.svg";

function ListItems() {
  const appState = useSelector((state) => state);
  const items = appState.items.items.values;
  const [id, setId] = useState("");
  const [open, setOpen] = useState(false);
  let history = useHistory();

  const handleOpenModal = (id) => {
    setOpen(true);
    document.body.style.overflow = "hidden";
    setId(id);
  };

  const handleCloseModal = () => {
    setOpen(false);
    document.body.style.overflow = "scroll";
  };

  return (
    <>
      <div className="container_global">
        <div className="container_all_items">
          {appState.auth.user.values &&
          appState.auth.user.values.role === "admin" ? (
            <Link to="/items/add">
              <div className="add_item">
                <Add className="icon_add" />
                <h3>Ajouter un item</h3>
              </div>
            </Link>
          ) : null}
          {items
            ? items.map((item, index) => {
                return (
                  <div className="all_items" key={index}>
                    <div
                      className="container_img"
                      onClick={() => {
                        history.push(`/item/${item.id}`);
                      }}
                      style={{
                        backgroundImage: `url(${item.headerAsset})`,
                      }}
                    >
                      <h3 className="title_item">{item.title.toUpperCase()}</h3>
                    </div>
                    {appState.auth.user.values &&
                    appState.auth.user.values.role === "admin" ? (
                      <div className="container_delete">
                        <img
                          src={edit}
                          alt="logo_trash"
                          className="logo_update"
                        />
                        <img
                          src={trash}
                          alt="logo_trash"
                          className="logo_delete"
                          onClick={() => handleOpenModal(item.id)}
                        />
                      </div>
                    ) : null}
                  </div>
                );
              })
            : null}
        </div>
      </div>
      {open ? (
        <Modal
          style={{ display: !open ? "none" : "block" }}
          text={"Êtes-vous sûr de vouloir supprimer cet item ?"}
          option={"SUPPRIMER"}
          path={`/items/${id}`}
          handleCloseModal={handleCloseModal}
        />
      ) : null}
    </>
  );
}

export default ListItems;

import React from "react";

import "../timeline/_timeline.scss";

const Timeline = ({ styleClassArray }) => {
  return (
    <div className="container_timeline">
      <div className="box_timeline_steps">
        <div className={`step ${styleClassArray[0]}`}>
          <h3>Item</h3>
        </div>
        <div className={`step ${styleClassArray[1]}`}>
          <h3>Constat</h3>
        </div>
        <div className={`step ${styleClassArray[2]}`}>
          <h3>Objectif</h3>
        </div>
        <div className={`step ${styleClassArray[3]}`}>
          <h3>Argument</h3>
        </div>
      </div>
      <div className="bar_timeline"></div>
    </div>
  );
};

export default Timeline;

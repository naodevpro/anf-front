import React, { useRef } from "react";
import Button from "../../atoms/Button/button";
import Input from "../../atoms/Input/input";

const FormObjectif = ({
  objectif,
  itemId,
  setObjectif,
  submit,
  scrollToTop,
}) => {
  const inputTitleObjectif = useRef();

  const handleSetObjectif = () => {
    setObjectif({
      ...objectif,
      titleObjectif: inputTitleObjectif.current.value,
    });
  };

  return (
    <form
      className="objectif_form"
      onSubmit={(e) => submit(e, "/objectifs", objectif, itemId)}
    >
      <div className="form_text_left">
        <Input
          reference={inputTitleObjectif}
          customClass={"input"}
          label={"Titre de l'objectif"}
          id={"titleObjectif"}
          name={"titleObjectif"}
          type={"text"}
          placeholder={"Titre de l'objectif"}
          height={"50px"}
          width={"100%"}
          onChangeMethod={handleSetObjectif}
        />
        <label htmlFor="descriptionObjectif">Description de l'objectif</label>
        <textarea
          id="descriptionObjectif"
          type="text"
          name="descriptionObjectif"
          placeholder="Description de l'objectif"
          onChange={(e) =>
            setObjectif({ ...objectif, descriptionObjectif: e.target.value })
          }
        />
      </div>
      <Button
        label={"Suivant"}
        height={"40px"}
        width={"100%"}
        type="submit"
        customClass={"btn basic-light-color primary-background"}
        onClickMethod={scrollToTop}
      />
    </form>
  );
};

export default FormObjectif;

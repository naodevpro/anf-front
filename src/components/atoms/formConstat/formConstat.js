import React, { useRef } from "react";
import Button from "../../atoms/Button/button";
import Input from "../../atoms/Input/input";

const FormConstat = ({
  form,
  constat,
  setConstat,
  itemId,
  submit,
  scrollToTop,
}) => {
  const inputAssetConstat = useRef({ value: null });
  const inputTitleConstat = useRef({ value: null });

  return (
    <form
      className="constat_form"
      ref={form}
      onSubmit={(e) => submit(e, "/constats")}
    >
      <div className="form_text_center">
        <label className="input_file_label">
          <p>+</p>
          <Input
            customClass={"input_file"}
            reference={inputAssetConstat}
            type={"file"}
            id={"assetConstat"}
            name={"assetConstat"}
            onChangeMethod={setConstat}
            onChangeParams={[
              { ...constat },
              { assetConstat: inputAssetConstat.current.value },
            ]}
          />
        </label>
      </div>
      <div className="form_text_left">
        <Input
          reference={inputTitleConstat}
          customClass={"input"}
          label={"Titre du constat"}
          type={"text"}
          id={"titleConstat"}
          name={"titleConstat"}
          placeholder={"Titre du constat"}
          height={"50px"}
          width={"100%"}
          onChangeMethod={setConstat}
          onChangeParams={[
            { ...constat },
            { titleConstat: inputTitleConstat.current.value },
          ]}
        />
        <Input
          customClass="input"
          label={"ID de l'item"}
          id={"itemId"}
          name={"itemId"}
          type={"text"}
          placeholder={"ID de l'item"}
          value={itemId}
          height={"50px"}
          width={"100%"}
          onChangeMethod={setConstat}
          onChangeParams={[{ ...constat }, { itemId: itemId }]}
        />
        <label htmlFor="contentConstat">Contenu du constat</label>
        <textarea
          id="contentConstat"
          type="text"
          name="contentConstat"
          placeholder="Contenu du constat"
          onChange={(e) =>
            setConstat({ ...constat, contentConstat: e.target.value })
          }
        />
      </div>
      <Button
        label={"Suivant"}
        height={"40px"}
        width={"100%"}
        type="submit"
        customClass={"btn basic-light-color primary-background"}
        onClickMethod={scrollToTop}
      />
    </form>
  );
};

export default FormConstat;

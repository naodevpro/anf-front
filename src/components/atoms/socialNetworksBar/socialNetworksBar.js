import React from "react";

import loudSpeaker from "../../../assets/icons/social-networks/loud_speaker.png";
import facebook from "../../../assets/icons/social-networks/facebook.png";
import twitter from "../../../assets/icons/social-networks/twitter.png";
import youtube from "../../../assets/icons/social-networks/youtube.png";
import instagram from "../../../assets/icons/social-networks/instagram.png";

import "./_socialNetworksBar.scss";

const SocialNetworksBar = () => {
  return (
    <div className="container_social_networks">
      <div className="heading_social_networks">
        <img src={loudSpeaker} alt="icon" />
        <h6 className="subtitle_heading">
          SUIVEZ L'ALLIANCE POUR UNE NOUVELLE FRANCE
        </h6>
      </div>
      <div className="box_social_networks">
        <div className="box_side_left">
          <img src={facebook} alt="icon" />
          <img src={twitter} alt="icon" />
        </div>
        <div className="box_side_right">
          <img src={youtube} alt="icon" />
          <img src={instagram} alt="icon" />
        </div>
      </div>
    </div>
  );
};

export default SocialNetworksBar;

import React from "react";
import Button from "../../atoms/Button/button";
import api from "../../../utils/api";

import "./_modal.scss";

const Modal = ({ text, option, path, handleCloseModal }) => {
  const refreshPage = () => {
    window.location.reload();
  };

  const handleRequest = async () => {
    try {
      switch (option) {
        case "SUPPRIMER":
          await api.delete(path);
          handleCloseModal();
          return refreshPage();
        case "AJOUTER":
          await api.post(path);
          handleCloseModal();
          return refreshPage();
        case "METTRE À JOUR":
          await api.put(path);
          handleCloseModal();
          return refreshPage();
        case "RÉCUPÉRER":
          await api.get(path);
          handleCloseModal();
          return refreshPage();
        default:
          break;
      }
    } catch (error) {
      // for debug
      return null;
    }
  };

  return (
    <div className="container_modal">
      <div className="modal">
        <div className="container_close" onClick={handleCloseModal}>
          <span>x</span>
        </div>
        <div className="container_actions">
          <h2>{text}</h2>
          <div className="all_actions">

            <Button
              label={"Annuler"}
              height={"50px"}
              width={"50%"}
              type="submit"
              customClass={
                "btn basic-light-color primary-background"
              }
              onClickMethod={handleCloseModal}
            />
            <Button
              label={option}
              height={"50px"}
              width={"50%"}
              type="submit"
              customClass={
                "btn basic-light-color primary-background"
              }
              onClickMethod={handleRequest}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;

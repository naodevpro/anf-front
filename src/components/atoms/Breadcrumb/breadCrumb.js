import React from "react";

import "./_breadCrumb.scss";
import Arrow from "../../../assets/icons/arrow_breadcrumb.svg";

function BreadCrumb({ steps, stepFocused }) {
  return (
    <div className="container">
      <div className="box_steps">
        {steps.map((step, index) => {
          return (
            <div className="box_step" key={index}>
              <h3
                className={
                  index === stepFocused ? "step m-1" : "step m-1 not_focused"
                }
              >
                {step.title}
              </h3>
              {index < steps.length - 1 ? (
                <img className="icon" src={Arrow} alt="arrow" />
              ) : null}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default BreadCrumb;

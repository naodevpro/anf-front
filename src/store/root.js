import { combineReducers } from "redux";

import mainReducer from "./main";
import authReducer from "./auth";
import itemsReducerCombined from "./items";
import itemReducerCombined from "./item";
import constatReducerCombined from "./constat";
import objectifReducerCombined from "./objectif";
import argumentReducerCombined from "./argument";
import paymentReducerCombined from "./payment";

const rootReducer = combineReducers({
  app: mainReducer,
  auth: authReducer,
  items: itemsReducerCombined,
  item: itemReducerCombined,
  constat: constatReducerCombined,
  objectif: objectifReducerCombined,
  argument: argumentReducerCombined,
  payment: paymentReducerCombined,
});

export default rootReducer;

import { combineReducers } from "redux";

const initialState = {
  values: null,
  isFetching: false,
  isSuccess: null,
};

const itemReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ITEM_FETCH":
      return { ...state, isFetching: true };
    case "ITEM_FETCH_SUCCESS":
      return { ...state, isFetching: false, isSuccess: true };
    case "ITEM_FETCH_WRONG":
      return { ...state, isFetching: false, isSuccess: false };
    case "ITEM_SET":
      return {
        ...state,
        values: action.payload,
      };
    case "ITEM_SET_ADMIN":
      return {
        ...state,
        values: action.payload,
      };
    case "ITEM_ERROR":
      return {
        values: action.payload,
        isFetching: false,
        isSuccess: false,
      };
    case "ITEM_RESET":
      return initialState;
    default:
      return state;
  }
};

const itemReducerCombined = combineReducers({
  item: itemReducer,
});

export default itemReducerCombined;

import { combineReducers } from "redux";

const initialState = {
  values: null,
  isFetching: false,
  isSuccess: null,
};

const paymentReducer = (state = initialState, action) => {
  switch (action.type) {
    case "PAYMENT_SET":
      return {
        ...state,
        isSuccess: true,
        values: action.payload,
      };
    case "PAYMENT_ERROR":
      return {
        isFetching: false,
        isSuccess: false,
        values: action.payload,
      };
    case "PAYMENT_RESET":
      return initialState;
    default:
      return state;
  }
};

const paymentReducerCombined = combineReducers({
  payment: paymentReducer,
});

export default paymentReducerCombined;

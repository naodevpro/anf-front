import { combineReducers } from "redux";

const initialState = {
  values: null,
  isFetching: false,
  isSuccess: null,
};

const constatReducer = (state = initialState, action) => {
  switch (action.type) {
    case "CONSTAT_SET":
      return {
        ...state,
        values: action.payload,
      };
    case "CONSTAT_ERROR":
      return {
        values: action.payload,
        isFetching: false,
        isSuccess: false,
      };
    case "CONSTAT_RESET":
      return initialState;
    default:
      return state;
  }
};

const constatReducerCombined = combineReducers({
  constat: constatReducer,
});

export default constatReducerCombined;

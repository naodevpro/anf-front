import { combineReducers } from "redux";

const initialState = {
  values: null,
  isFetching: false,
  isSuccess: null,
};

const itemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ITEMS_FETCH":
      return { ...state, isFetching: true };
    case "ITEMS_FETCH_SUCCESS":
      return { ...state, isFetching: false, isSuccess: true };
    case "ITEMS_FETCH_WRONG":
      return { ...state, isFetching: false, isSuccess: false };
    case "ITEMS_SET":
      return {
        ...state,
        values: action.payload,
      };
    case "ITEMS_RESET":
      return initialState;
    default:
      return state;
  }
};

const itemsReducerCombined = combineReducers({
  items: itemsReducer,
});

export default itemsReducerCombined;

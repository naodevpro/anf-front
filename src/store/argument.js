import { combineReducers } from "redux";

const initialState = {
  values: null,
  isSuccess: null,
};

const argumentReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ARGUMENT_SET":
      return {
        ...state,
        values: action.payload,
        isSuccess: true,
      };
    case "ARGUMENT_ERROR":
      return {
        values: action.payload,
        isSuccess: false,
      };
    case "ARGUMENT_RESET":
      return initialState;
    default:
      return state;
  }
};

const argumentReducerCombined = combineReducers({
  argument: argumentReducer,
});

export default argumentReducerCombined;

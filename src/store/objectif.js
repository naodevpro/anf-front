import { combineReducers } from "redux";

const initialState = {
  values: null,
  isSuccess: null,
};

const objectifReducer = (state = initialState, action) => {
  switch (action.type) {
    case "OBJECTIF_SET":
      return {
        ...state,
        values: action.payload,
        isSuccess: true,
      };
    case "OBJECTIF_ERROR":
      return {
        values: action.payload,
        isSuccess: false,
      };
    case "OBJECTIF_RESET":
      return initialState;
    default:
      return state;
  }
};

const objectifReducerCombined = combineReducers({
  objectif: objectifReducer,
});

export default objectifReducerCombined;

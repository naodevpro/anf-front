import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";

import api from "./utils/api";
import ScrollToTop from "./utils/scrollToTop";

import Layout from "./components/layout/layout";
import Home from "./components/pages/home/home";
import Items from "./components/pages/items/items";
import Item from "./components/pages/item/item";
import LegalNotices from "./components/pages/legalNotices/legalNotices";
import Login from "./components/pages/login/login";
import AddItem from "./components/pages/addItem/addItem";
import CreateItem from "./components/pages/createItem/createItem";
import JoinUs from "./components/pages/joinUs/joinUs";
import Donation from "./components/pages/donation/donation";
import SuccessPayment from "./components/pages/successPayment/successPayment";
import ForgetPassword from "./components/pages/forgetPassword/forgetPassword";
import ResetPassword from "./components/pages/resetPassword/resetPassword";

const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_KEY);

function App() {
  const appState = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    const appInit = () => {
      dispatch({ type: "APP_INIT" });
      dispatch({ type: "APP_READY" });
    };
    appInit();
  }, [dispatch]);

  useEffect(() => {
    const getAuth = async () => {
      try {
        dispatch({ type: "USER_FETCH" });
        let user = await api.get("/me");
        dispatch({ type: "USER_FETCH_SUCCESS" });
        dispatch({ type: "USER_SET", payload: user.data.data });
      } catch (error) {
        dispatch({
          type: "USER_FETCH_WRONG",
          payload: error.response.data.message.message,
        });
      }
    };
    getAuth();
  }, [dispatch]);

  useEffect(() => {
    const getItems = async () => {
      try {
        dispatch({ type: "ITEMS_FETCH" });
        let items = await api.get("/items");
        dispatch({ type: "ITEMS_FETCH_SUCCESS" });
        dispatch({ type: "ITEMS_SET", payload: items.data.data });
      } catch (err) {
        dispatch({ type: "ITEMS_RESET" });
      }
    };
    getItems();
  }, [dispatch]);

  if (appState.app.loading) {
    return (
      <div>
        <h1>Loading...</h1>
      </div>
    );
  }

  return (
    <div className="App">
      <Router>
        <ScrollToTop />
        <Layout>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/items" component={Items} />
            <Route exact path="/item/:id" component={Item} />
            <Route exact path="/legalNotices" component={LegalNotices} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/items/add" component={AddItem} />
            <Route exact path="/items/create" component={CreateItem} />
            <Route exact path="/join" component={JoinUs} />
            <Route path="/donation/:amount">
              <div className="stripe-box">
                <Elements stripe={stripePromise}>
                  <Donation />
                </Elements>
              </div>
            </Route>
            {/* <Route path="/payment/:amount">
              <div className="stripe-box">
                <Elements stripe={stripePromise}>
                  <Payment />
                </Elements>
              </div>
            </Route> */}
            <Route exact path="/success/payment" component={SuccessPayment} />
            <Route exact path="/forget/password" component={ForgetPassword} />
            <Route exact path="/reset/password" component={ResetPassword} />
            <Redirect from="*" to="/" />
          </Switch>
        </Layout>
      </Router>
    </div>
  );
}

export default App;

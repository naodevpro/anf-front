import BoxNotification from "../components/atoms/boxNotification/boxNotification";

const renderNotification = (
    code,
    titleNotification,
    descriptionNotification,
    handleClose
  ) => {
    return (
      <BoxNotification
        code={code}
        titleNotification={titleNotification}
        descriptionNotification={descriptionNotification}
        handleClose={handleClose}
      />
    );
  };

  export default renderNotification;

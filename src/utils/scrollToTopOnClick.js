export const scrollToTopOnClick = () => {
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
};

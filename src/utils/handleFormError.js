import { toggleClass } from "./changeClass";

export const handleFormError = (e, fields, field, setFields, index) => {
  const regexp = new RegExp(field.regexp);
  if (!regexp.test(field.reference.current.value)) {
    setFields(
      fields.map((field) =>
        field.id === index ? { ...field, error: true } : field
      )
    );
    toggleClass(field.reference, "error-border", "success-border");
  } else {
    setFields(
      fields.map((field) =>
        field.id === index ? { ...field, error: false } : field
      )
    );
    toggleClass(field.reference, "success-border", "error-border");
  }
};

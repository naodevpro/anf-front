export const toggleClass =  (reference, classAdded, classRemoved) => {
    reference.current.classList.add(classAdded)
    reference.current.classList.remove(classRemoved)
}

export const addClass =  (reference, classAdded) => {
    reference.current.classList.add(classAdded)
}

export const removeClass =  (reference, classRemoved) => {
    reference.current.classList.remove(classRemoved)
}

